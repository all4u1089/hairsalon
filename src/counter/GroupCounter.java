package counter;

import java.util.List;
import java.util.Vector;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import vn.toancauxanh.gg.model.QSessionCount;
import vn.toancauxanh.gg.model.SessionCount;
import vn.toancauxanh.service.BasicService;

public class GroupCounter extends BasicService<SessionCount> {
	private List<Counter> counters;
	private static final String SIGNAL = "~~~";
	
	public GroupCounter() {
		// TODO Auto-generated constructor stub
		counters = new Vector<>();
	}

	public void sendMessage(String sender, String message) {
		broadcast(sender, sender + ":" + message);
	}

	/**
	 * send messages to all counter except sender
	 *
	 * @param sender
	 * @param message
	 */
	public void broadcast(String sender, String message) {
		synchronized (counters) {
			for (Counter _counter : counters) {
				if (!_counter.get_countName().equals(sender))
					_counter.addMessage(message);
			}
		}
	}

	public void subscribe(Counter counter) {
		//HttpSession nativeSession = (HttpSession) Executions.getCurrent().getSession().getNativeSession();
		synchronized (counters) {
			
			counters.add(counter);
			//System.out.println("size : " + counters.size());
		}
		
		try {
			
			transactioner().execute(new TransactionCallbackWithoutResult() {
				@Override
				protected void doInTransactionWithoutResult(TransactionStatus arg0) {
					SessionCount exist = find(SessionCount.class)
							.where(QSessionCount.sessionCount.trangThai.eq(core().TT_AP_DUNG)).fetchFirst();
					
					if (exist == null) {
						
						exist = new SessionCount(String.valueOf(counters.size()));
						//exist.setNgayTao(new Date(nativeSession.getCreationTime()));
						//exist.setNgaySua(new Date(nativeSession.getLastAccessedTime()));
					} else {
						
						exist.setSessionId(String.valueOf(counters.size()));
						//exist.setNgaySua(new Date(nativeSession.getLastAccessedTime()));
					}
					
					exist.doSave();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		broadcast(counter.get_countName(), SIGNAL + counter.get_countName() + " has joined this group" + SIGNAL);
	}

	public void unsubscribe(Counter counter) {
		synchronized (counters) {
			//System.out.println("Remove");
			counters.remove(counter);
		}

		//System.out.println("Size : " + counters.size());
		
		try {
			//HttpSession nativeSession = (HttpSession) Executions.getCurrent().getSession().getNativeSession();
			transactioner().execute(new TransactionCallbackWithoutResult() {
				@Override
				protected void doInTransactionWithoutResult(TransactionStatus arg0) {
					SessionCount exist = find(SessionCount.class)
							.where(QSessionCount.sessionCount.trangThai.eq(core().TT_AP_DUNG)).fetchFirst();

					if (exist != null) {

						exist.setSessionId(String.valueOf(counters.size()));
						//exist.setNgaySua(new Date(nativeSession.getLastAccessedTime()));
						exist.doSave();
					}

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

		broadcast(counter.get_countName(), SIGNAL + counter.get_countName() + " has left the group" + SIGNAL);
	}
}
