package counter;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.zkoss.lang.Threads;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.DesktopUnavailableException;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Window;

public class Counter extends Thread {
	private static final Logger log = Logger.getLogger(Counter.class.getName());
	private boolean _ceased;
	private GroupCounter _groupCounter;
	private final Desktop _desktop;
	private String _countName;
	private String _msg;

	public Counter(GroupCounter _groupCounter1, String _countName1, Desktop _desktop1) {
		super();
		this._groupCounter = _groupCounter1;
		this._desktop = _desktop1;
		this._countName = _countName1;
		this._msg = "";
	}

	@Override
	public void run() {
		if (!_desktop.isServerPushEnabled())
			_desktop.enableServerPush(true);
		log.info("Active counter thread: " + getName());
		_groupCounter.subscribe(this);
		try {
			while (!_ceased) {
				try {
					if (_msg.compareTo("") == 0) {
						Threads.sleep(500);// Update each 0.5 seconds
					} else {
						Executions.activate(_desktop);
						try {
							process();
						} finally {
							Executions.deactivate(_desktop);
						}
					}
				} catch (DesktopUnavailableException ex) {
					ex.printStackTrace();
					log.info("Browser exited.");
					logout();
				} catch (Throwable ex) {
					log.log(Level.SEVERE, ex.getMessage(), ex);
					throw UiException.Aide.wrap(ex);
				}
			}
		} finally {
			logout();
		}
		log.info("counter thread ceased: " + getName());
	}

	private void process() {
		if (_msg.compareTo("") != 0) {
			HashMap<String, String> msgs = new HashMap<>();
			msgs.put("msg", _msg);
			Window win = (Window) Path.getComponent("/win");
			Events.postEvent(new Event("onSend", win, msgs));
			_msg = "";// reset message
		}
	}

	public void logout() {
		setDone();
		//log.info(get_countName() + " has logged out of the group!");
		//System.out.println(get_countName() + " has logged out of the group!");
		_groupCounter.unsubscribe(this);
		if (_desktop.isServerPushEnabled())
			Executions.getCurrent().getDesktop().enableServerPush(false);
	}

	/**
	 * stop this thread
	 */
	public void setDone() {
		_ceased = true;
	}

	public void addMessage(String message) {
		_msg = message;
	}

	public String get_countName() {
		return _countName;
	}
}
