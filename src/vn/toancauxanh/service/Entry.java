
package vn.toancauxanh.service;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Object;

import vn.toancauxanh.cms.service.KhachHangService;
import vn.toancauxanh.cms.service.KieuTocService;
import vn.toancauxanh.cms.service.LichSuKhachHangService;
import vn.toancauxanh.cms.service.LoaiChiTieuService;
import vn.toancauxanh.cms.service.MaMauService;
import vn.toancauxanh.cms.service.ThoCatTocService;
import vn.toancauxanh.cms.service.ChiPhiService;
import vn.toancauxanh.cms.service.ChiTietDichVuService;
import vn.toancauxanh.cms.service.GiaTienBamPhongService;
import vn.toancauxanh.cms.service.GiaTienCatTocService;
import vn.toancauxanh.cms.service.GiaTienDuoiService;
import vn.toancauxanh.cms.service.GiaTienGoiService;
import vn.toancauxanh.cms.service.GiaTienNangNenService;
import vn.toancauxanh.cms.service.GiaTienPhucHoiService;
import vn.toancauxanh.cms.service.GiaTienTayTocService;
import vn.toancauxanh.cms.service.GiaTienUonDuoiNhuomTocNamService;
import vn.toancauxanh.cms.service.GioiTinhService;
import vn.toancauxanh.model.VaiTro;

@Configuration
@Controller
public class Entry extends BaseObject<Object> {
	static Entry instance;

	@Value("${trangthai.apdung}")
	public String TT_AP_DUNG = "";
	@Value("${trangthai.daxoa}")
	public String TT_DA_XOA = "";
	@Value("${trangthai.khongapdung}")
	public String TT_KHONG_AP_DUNG = "";	

	@Value("${action.xem}")
	public String XEM = ""; // duoc xem bat ky
	@Value("${action.list}")
	public String LIST = ""; // duoc vao trang list search url
	@Value("${action.sua}")
	public String SUA = ""; // duoc sua bat ky
	@Value("${action.xoa}")
	public String XOA = ""; // duoc xoa bat ky
	@Value("${action.them}")
	public String THEM = ""; // duoc them
	
	@Value("${url.nguoidung}")
	public String NGUOIDUNG = "";
	@Value("${url.vaitro}")
	public String VAITRO = "";
	@Value("${url.hethong}")
	public String HETHONG = "";
	@Value("${url.gioitinh}")
	public String GIOITINH = "";
	@Value("${url.khachhang}")
	public String KHACHHANG = "";
	@Value("${url.quantrihethong}")
	public String QUANTRIHETHONG = "";
	@Value("${url.thongke}")
	public String THONGKE = "";
	
	// uend
	public char CHAR_CACH = ':';
	public String CACH = CHAR_CACH + "";
	
	@Value("${url.vaitro}" + ":" + "${action.xem}")
	public String VAITROXEM;
	@Value("${url.vaitro}" + ":" + "${action.them}")
	public String VAITROTHEM = "";
	@Value("${url.vaitro}" + ":" + "${action.list}")
	public String VAITROLIST = "";
	@Value("${url.vaitro}" + ":" + "${action.xoa}")
	public String VAITROXOA = "";
	@Value("${url.vaitro}" + ":" + "${action.sua}")
	public String VAITROSUA = "";
	
	@Value("${url.nguoidung}" + ":" + "${action.xem}")
	public String NGUOIDUNGXEM = "";
	@Value("${url.nguoidung}" + ":" + "${action.them}")
	public String NGUOIDUNGTHEM = "";
	@Value("${url.nguoidung}" + ":" + "${action.list}")
	public String NGUOIDUNGLIST = "";
	@Value("${url.nguoidung}" + ":" + "${action.xoa}")
	public String NGUOIDUNGXOA = "";
	@Value("${url.nguoidung}" + ":" + "${action.sua}")
	public String NGUOIDUNGSUA = "";
			
	@Value("${url.gioitinh}" + ":" + "${action.xem}")
	public String GIOITINHXEM = "";
	@Value("${url.gioitinh}" + ":" + "${action.them}")
	public String GIOITINHTHEM = "";
	@Value("${url.gioitinh}" + ":" + "${action.list}")
	public String GIOITINHLIST = "";
	@Value("${url.gioitinh}" + ":" + "${action.xoa}")
	public String GIOITINHXOA = "";
	@Value("${url.gioitinh}" + ":" + "${action.sua}")
	public String GIOITINHSUA = "";
	
	@Value("${url.khachhang}" + ":" + "${action.xem}")
	public String KHACHHANGXEM = "";
	@Value("${url.khachhang}" + ":" + "${action.them}")
	public String KHACHHANGTHEM = "";
	@Value("${url.khachhang}" + ":" + "${action.list}")
	public String KHACHHANGLIST = "";
	@Value("${url.khachhang}" + ":" + "${action.xoa}")
	public String KHACHHANGXOA = "";
	@Value("${url.khachhang}" + ":" + "${action.sua}")
	public String KHACHHANGSUA = "";
		
	@Value("${url.quantrihethong}" + ":" + "${action.list}")
	public String QUANTRIHETHONGLIST = "";
	
	// Hệ thống active
	@Value("${url.hethong}" + ":" + "${action.xem}")
	public String HETHONGXEM = "";
	@Value("${url.hethong}" + ":" + "${action.sua}")
	public String HETHONGSUA = "";	
	
	// aend
	public String[] getRESOURCES() {
		return new String[] {KHACHHANG,
				NGUOIDUNG, VAITRO, GIOITINH, QUANTRIHETHONG, THONGKE};
	}

	public String[] getACTIONS() {
		return new String[] { LIST, XEM, THEM, SUA, XOA};
	}

	static {
		File file = new File(Labels.getLabel("filestore.root") + File.separator + Labels.getLabel("filestore.folder"));
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory mis is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
	}
	@Autowired
	public Environment env;

	@Autowired
	DataSource dataSource;

	public Entry() {
		super();
		setCore();
		instance = this;
	}

	@Bean
	public FilterRegistrationBean cacheFilter() {
		FilterRegistrationBean rs = new FilterRegistrationBean(new CacheFilter());
		rs.addUrlPatterns("*.css");
		rs.addUrlPatterns("*.js");
		rs.addUrlPatterns("*.wpd");
		rs.addUrlPatterns("*.wcs");
		rs.addUrlPatterns("*.jpg");
		rs.addUrlPatterns("*.jpeg");
		rs.addUrlPatterns("*.png");
		return rs;
	}

//	@Bean
//	public FilterRegistrationBean loginFilter() {
//		FilterRegistrationBean rs = new FilterRegistrationBean(new LoginFilter());
//		rs.addUrlPatterns("/*");
//		return rs;
//	}
	
	@RequestMapping(value = "/")
	public String cp() {
		return "forward:/WEB-INF/zul/home1.zul?resource=khachhang&action=lietke&file=/WEB-INF/zul/khachhang/list.zul";
	}

	@RequestMapping(value = "/{path:.+$}")
	public String cp(@PathVariable String path) {
		if (path.equals("khachhang")) {
			return cp();
		}
		return "forward:/WEB-INF/zul/home1.zul?resource=" + path + "&action=lietke&file=/WEB-INF/zul/" + path
				+ "/list.zul";
	}
	
	@RequestMapping(value = "/{path:.+$}/them")
	public String actionThem(@PathVariable String path) {
		return "forward:/WEB-INF/zul/home1.zul?resource=" + path + "&action=them&file=/WEB-INF/zul/khachhang/them.zul";
	}
	
	@RequestMapping(value = "/{path:.+$}/xem/{id:.+$}")
	public String actionXem(@PathVariable String path, @PathVariable Long id) {
		return "forward:/WEB-INF/zul/home1.zul?resource=" + path + "&action=xem&file=/WEB-INF/zul/khachhang/xem.zul&id=" + id;
	}
	
	@RequestMapping(value = "/{path:.+$}/sua/{id:.+$}")
	public String actionSua(@PathVariable String path, @PathVariable Long id) {
		return "forward:/WEB-INF/zul/home1.zul?resource=" + path + "&action=sua&file=/WEB-INF/zul/khachhang/sua.zul&id=" + id;
	}
	
	@RequestMapping(value = "/thongke/{path:.+$}")
	public String thongke(@PathVariable String path) {
		return "forward:/WEB-INF/zul/home1.zul?resource=thongke&action=" + path + "&file=/WEB-INF/zul/thongke/" + path + ".zul";
	}
	
	@RequestMapping(value = "/login")
	public String dangNhapBackend() {
		return "forward:/WEB-INF/zul/login.zul";
	}
	
	public final GioiTinhService getGioiTinhs() {
		return new GioiTinhService();
	}
	
	public final MaMauService getMaMaus() {
		return new MaMauService();
	}
	
	public final KieuTocService getKieuTocs() {
		return new KieuTocService();
	}
	
	public final GiaTienCatTocService getGiaTienCatTocs() {
		return new GiaTienCatTocService();
	}
	
	public final GiaTienPhucHoiService getGiaTienPhucHois() {
		return new GiaTienPhucHoiService();
	}
	
	public final GiaTienGoiService getGiaTienGois() {
		return new GiaTienGoiService();
	}
	
	public final GiaTienDuoiService getGiaTienDuois() {
		return new GiaTienDuoiService();
	}
	
	public final GiaTienBamPhongService getGiaTienBamPhongs() {
		return new GiaTienBamPhongService();
	}
	
	public final GiaTienUonDuoiNhuomTocNamService getGiaTienUonDuoiNhuomTocNams() {
		return new GiaTienUonDuoiNhuomTocNamService();
	}
	
	public final GiaTienNangNenService getGiaTienNangNens() {
		return new GiaTienNangNenService();
	}
	
	public final GiaTienTayTocService getGiaTienTayTocs() {
		return new GiaTienTayTocService();
	}
	
	public final LichSuKhachHangService getLichSuKhachHangs() {
		return new LichSuKhachHangService();
	}
	
	public final ChiTietDichVuService getChiTietDichVus() {
		return new ChiTietDichVuService();
	}
	
	public final KhachHangService getKhachHangs() {
		return new KhachHangService();
	}
	
	public final ThoCatTocService getThoCatTocs() {
		return new ThoCatTocService();
	}

	public final Quyen getQuyen() {
		return getNhanVien().getTatCaQuyen();
	}
	
	public final ChiPhiService getChiPhis() {
		return new ChiPhiService();
	}
	
	public final LoaiChiTieuService getLoaiChiTieus() {
		return new LoaiChiTieuService();
	}

	public final VaiTroService getVaiTros() {
		return new VaiTroService();
	}
	public final List<String> getNoiDungActive(){
		return Arrays.asList("chude", "baiviet", "video", "gallery", "lienket", "linhvuchoidap", "hoidaptructuyen", "faqcategory", "faq");
	}
	
	public boolean checkVaiTro(String vaiTro){
		if(vaiTro==null || vaiTro.isEmpty()){
			return false;
		}
		boolean rs = false;
		for (VaiTro vt : getNhanVien().getVaiTros()) {
			if(vaiTro.equals(vt.getAlias())){
				rs = true;
				break;
			}
		}
		return rs;// || getQuyen().get(vaiTro);
	}
		
}