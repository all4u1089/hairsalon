package vn.toancauxanh.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import vn.toancauxanh.gg.model.KhachHang;
import vn.toancauxanh.gg.model.LichSuKhachHang;
import vn.toancauxanh.gg.model.QKhachHang;
import vn.toancauxanh.gg.model.QLichSuKhachHang;
import vn.toancauxanh.gg.model.ThongBaoNguoiDung;
import vn.toancauxanh.model.NhanVien;
import vn.toancauxanh.model.QNhanVien;
import vn.toancauxanh.model.Setting;

@Component
public class ScheduledTasks extends BasicService<Object> {

	//second, minute, hour, day of month, month, day(s) of week
	@Scheduled(cron = "0 37 16 * * *")
	public void updateHinhThucXuLyQuanLy() throws Exception {
		
	}
	
	@Scheduled(cron = "0 00 8 * * *")
	public void updateKiemTraSinhNhat() throws Exception { 
		System.out.println("updateKiemTraSinhNhat");
		Setting setting = find(Setting.class).fetchFirst();		
		Calendar cal = Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		List<KhachHang> list = find(KhachHang.class)
				.where(QKhachHang.khachHang.ngaySinh.dayOfMonth().eq(cal.get(Calendar.DAY_OF_MONTH)))
				.where(QKhachHang.khachHang.ngaySinh.month().eq(cal.get(Calendar.MONTH) + 1))
				.fetch();
		List<NhanVien> listNhanVien = find(NhanVien.class).where(QNhanVien.nhanVien.trangThai.eq(core().TT_AP_DUNG)).fetch();
		for (KhachHang kh : list) {
			for (NhanVien nv: listNhanVien) {
				ThongBaoNguoiDung tb = new ThongBaoNguoiDung();
				tb.setCanBo(nv);
				tb.setKhachHang(kh);
				String thongBao = "Hôm nay (" + df.format(kh.getNgaySinh()) + ") là sinh nhật của khách hàng " + kh.getHoVaTen();
				tb.setThongBao(thongBao);
				tb.saveNotShowNotification();
				if (setting.getDiaChiEmail() != null && !setting.getDiaChiEmail().isEmpty()) {
					thongBao = thongBao.concat(". Chi tiết thông tin khách hàng, truy cập: http://trinhsalon-trinh123.a3c1.starter-us-west-1.openshiftapps.com/khachhang/xem/" + kh.getId());
					SendEmail.sendEmailGmail(setting.getDiaChiEmail(), "Thong bao sinh nhat khach hang", thongBao);
				}
			}
		}
		
		//
		Calendar calDichVu = Calendar.getInstance();
		calDichVu.add(Calendar.DATE, -7);
		List<LichSuKhachHang> listLichSu = find(LichSuKhachHang.class)
				.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.dayOfMonth().eq(calDichVu.get(Calendar.DAY_OF_MONTH)))
				.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.month().eq(calDichVu.get(Calendar.MONTH) + 1))
				.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.year().eq(calDichVu.get(Calendar.YEAR)))
				.fetch();
		for (LichSuKhachHang ls : listLichSu) {
			for (NhanVien nv: listNhanVien) {
				ThongBaoNguoiDung tb = new ThongBaoNguoiDung();
				tb.setCanBo(nv);
				tb.setKhachHang(ls.getKhachHang());
				String thongBao = "Khách hàng " + ls.getKhachHang().getHoVaTen() + " đã sử dụng dịch vụ vào ngày " + df.format(ls.getNgaySuDung()) + " cần chăm sóc";
				tb.setThongBao(thongBao);
				tb.saveNotShowNotification();
				if (setting.getDiaChiEmail() != null && !setting.getDiaChiEmail().isEmpty()) {
					thongBao = thongBao.concat(". Chi tiết thông tin khách hàng, truy cập: http://trinhsalon-trinh123.a3c1.starter-us-west-1.openshiftapps.com/khachhang/xem/" + ls.getKhachHang().getId());
					SendEmail.sendEmailGmail(setting.getDiaChiEmail(), "Thong bao cham soc khach hang", thongBao);
				}				
			}
		}
	}

}
