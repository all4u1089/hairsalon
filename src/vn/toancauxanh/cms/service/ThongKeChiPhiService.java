package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.google.charts.PieChart;
import org.zkoss.google.charts.data.DataTable;
import org.zkoss.zul.Div;

import vn.toancauxanh.gg.model.ChiPhi;
import vn.toancauxanh.gg.model.LoaiChiTieu;
import vn.toancauxanh.gg.model.QChiPhi;
import vn.toancauxanh.service.BasicService;

public class ThongKeChiPhiService extends BasicService<Object>{
		
	private Date ngayBatDau;
	private Date ngayKetThuc;
	private List<Object[]> listObject = new ArrayList<Object[]>();
	private boolean flagThongKe;
	
	public Date getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public Date getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	
	public List<Object[]> getListObject() {
		return listObject;
	}
	public void setListObject(List<Object[]> listObject) {
		this.listObject = listObject;
	}
	public boolean isFlagThongKe() {
		return flagThongKe;
	}
	public void setFlagThongKe(boolean flagThongKe) {
		this.flagThongKe = flagThongKe;
	}
	@Command
	public void thongKe(@BindingParam("div") final Div chartArea) {
		flagThongKe = true;
		listObject.clear();
		if (ngayBatDau != null && ngayKetThuc != null) {
			Calendar calKetThuc = Calendar.getInstance();
			calKetThuc.setTime(ngayKetThuc);
			calKetThuc.set(Calendar.HOUR, 16);
			calKetThuc.set(Calendar.MINUTE, 59);
			long tongSo = 0;
			Object[] obj = null;
			for (LoaiChiTieu loaiChiTieu : core().getLoaiChiTieus().getListLoaiChiTieu()) {
				obj = new Object[2];				
				obj[0] = loaiChiTieu.getTen();
				long tongBanHang = 0;
				try {
					tongBanHang = find(ChiPhi.class)
							.where(QChiPhi.chiPhi.loaiChiTieu.eq(loaiChiTieu))
							.where(QChiPhi.chiPhi.ngayPhatSinh.between(ngayBatDau, calKetThuc.getTime()))
							.select(QChiPhi.chiPhi.giaTien.sum())
							.fetchFirst();
				} catch (Exception ex) {}					
				obj[1] = tongBanHang;
				tongSo += (long)obj[1];
				listObject.add(obj);
			}
			obj = new Object[2];
			obj[0] = "Tổng";
			obj[1] = tongSo;
			listObject.add(obj);
			for (Object[] dv : listObject) {
				System.out.println("dv: " + dv[0] + " _ " + dv[1]);
			}
		} else if (ngayBatDau != null) {
			long tongSo = 0;
			Object[] obj = null;
			for (LoaiChiTieu loaiChiTieu : core().getLoaiChiTieus().getListLoaiChiTieu()) {
				obj = new Object[2];				
				obj[0] = loaiChiTieu.getTen();
				long tongBanHang = 0;
				try {
					tongBanHang = find(ChiPhi.class)
							.where(QChiPhi.chiPhi.loaiChiTieu.eq(loaiChiTieu))
							.where(QChiPhi.chiPhi.ngayPhatSinh.after(ngayBatDau))
							.select(QChiPhi.chiPhi.giaTien.sum())
							.fetchFirst();
				} catch (Exception ex) {}					
				obj[1] = tongBanHang;
				tongSo += (long)obj[1];
				listObject.add(obj);
			}
			obj = new Object[2];
			obj[0] = "Tổng";
			obj[1] = tongSo;
			listObject.add(obj);
		} else if (ngayKetThuc != null) {
			Calendar calKetThuc = Calendar.getInstance();
			calKetThuc.setTime(ngayKetThuc);
			calKetThuc.set(Calendar.HOUR, 16);
			calKetThuc.set(Calendar.MINUTE, 59);
			long tongSo = 0;
			Object[] obj = null;
			for (LoaiChiTieu loaiChiTieu : core().getLoaiChiTieus().getListLoaiChiTieu()) {
				obj = new Object[2];				
				obj[0] = loaiChiTieu.getTen();
				long tongBanHang = 0;
				try {
					tongBanHang = find(ChiPhi.class)
							.where(QChiPhi.chiPhi.loaiChiTieu.eq(loaiChiTieu))
							.where(QChiPhi.chiPhi.ngayPhatSinh.before(calKetThuc.getTime()))
							.select(QChiPhi.chiPhi.giaTien.sum())
							.fetchFirst();
				} catch (Exception ex) {}					
				obj[1] = tongBanHang;
				tongSo += (long)obj[1];
				listObject.add(obj);
			}
			obj = new Object[2];
			obj[0] = "Tổng";
			obj[1] = tongSo;
			listObject.add(obj);
		} else {
			long tongSo = 0;
			Object[] obj = null;
			for (LoaiChiTieu loaiChiTieu : core().getLoaiChiTieus().getListLoaiChiTieu()) {
				obj = new Object[2];				
				obj[0] = loaiChiTieu.getTen();
				long tongBanHang = 0;
				try {
					tongBanHang = find(ChiPhi.class)
							.where(QChiPhi.chiPhi.loaiChiTieu.eq(loaiChiTieu))
							.select(QChiPhi.chiPhi.giaTien.sum())
							.fetchFirst();
				} catch (Exception ex) {}					
				obj[1] = tongBanHang;
				tongSo += (long)obj[1];
				listObject.add(obj);
			}
			obj = new Object[2];
			obj[0] = "Tổng";
			obj[1] = tongSo;
			listObject.add(obj);
		}
		showChart(chartArea);
		BindUtils.postNotifyChange(null, null, this, "listObject");
		BindUtils.postNotifyChange(null, null, this, "flagThongKe");
	}
	
	public void showChart(Div chartArea) {
		//System.out.println("okok: " + chartArea);
		DataTable data = new DataTable();
		data.addStringColumn("Task", "task");
		data.addNumberColumn("Hours per Day", "hours");
		for (Object[] obj : listObject) {
			if (!obj[0].equals("Tổng")) {
				data.addRow(obj[0], obj[1]);
			}			
		}
		Map<String, Object> area = new HashMap<String, Object>();
        area.put("width", "92%");
        area.put("height", "92%");
        area.put("left", "40");    
		PieChart chart = new PieChart();
		chart.setTitle("Nhân khẩu toàn thành phố");
		chart.setHeight("300px");
		chart.setOption("chartArea", area);
		chart.setData(data);
		chart.setOption("pieHole", 0.6);
		chartArea.appendChild(chart);
	}
}
