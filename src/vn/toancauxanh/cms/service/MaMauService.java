package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.MaMau;
import vn.toancauxanh.gg.model.QMaMau;
import vn.toancauxanh.service.BasicService;

public class MaMauService extends BasicService<MaMau>{
			
	public List<MaMau> getListMaMau() {
		List<MaMau> list = new ArrayList<MaMau>();
		list = find(MaMau.class)
				.where(QMaMau.maMau.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QMaMau.maMau.ten.asc())
				.fetch();
		return list;
	}
	
	public List<MaMau> getListMaMauAndNull() {
		List<MaMau> list = new ArrayList<MaMau>();
		list.add(null);
		list.addAll(getListMaMau());
		return list;
	}
	
	public JPAQuery<MaMau> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<MaMau> phongBan = find(MaMau.class)
				.where(QMaMau.maMau.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QMaMau.maMau.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QMaMau.maMau.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QMaMau.maMau.ngaySua.desc());
		return phongBan;
	}
}
