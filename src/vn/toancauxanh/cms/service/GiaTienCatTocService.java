package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.GiaTienCatToc;
import vn.toancauxanh.gg.model.QGiaTienCatToc;
import vn.toancauxanh.service.BasicService;

public class GiaTienCatTocService extends BasicService<GiaTienCatToc>{
		
	@Init
	public void init() {
		List<GiaTienCatToc> list = find(GiaTienCatToc.class)
				.where(QGiaTienCatToc.giaTienCatToc.trangThai.ne(core().TT_DA_XOA))
				.fetch();
		if (list == null || list.isEmpty()) {
			bootstrapGiaTienCatToc();
		}
	}
	
	public List<GiaTienCatToc> getListGiaTienCatToc() {
		List<GiaTienCatToc> list = new ArrayList<GiaTienCatToc>();
		list = find(GiaTienCatToc.class)
				.where(QGiaTienCatToc.giaTienCatToc.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QGiaTienCatToc.giaTienCatToc.ten.asc())
				.fetch();
		return list;
	}
	
	public List<GiaTienCatToc> getListGiaTienCatTocAndNull() {
		List<GiaTienCatToc> list = new ArrayList<GiaTienCatToc>();
		list.add(null);
		list.addAll(getListGiaTienCatToc());
		return list;
	}
	
	public JPAQuery<GiaTienCatToc> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<GiaTienCatToc> phongBan = find(GiaTienCatToc.class)
				.where(QGiaTienCatToc.giaTienCatToc.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QGiaTienCatToc.giaTienCatToc.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QGiaTienCatToc.giaTienCatToc.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QGiaTienCatToc.giaTienCatToc.ngaySua.desc());
		return phongBan;
	}
	
	public void bootstrapGiaTienCatToc() {
		GiaTienCatToc giaTien = new GiaTienCatToc();
		giaTien.setTen("Nữ ngắn");
		giaTien.setGiaTien(150000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienCatToc();
		giaTien.setTen("Nữ dài");
		giaTien.setGiaTien(100000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienCatToc();
		giaTien.setTen("Nam");
		giaTien.setGiaTien(50000);
		giaTien.saveNotShowNotification();
	}
}
