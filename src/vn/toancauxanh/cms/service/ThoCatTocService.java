package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;
import vn.toancauxanh.gg.model.QThoCatToc;
import vn.toancauxanh.gg.model.ThoCatToc;
import vn.toancauxanh.service.BasicService;

public class ThoCatTocService extends BasicService<ThoCatToc>{
			
	public List<ThoCatToc> getListThoCatToc() {
		List<ThoCatToc> list = new ArrayList<ThoCatToc>();
		list = find(ThoCatToc.class)
				.where(QThoCatToc.thoCatToc.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QThoCatToc.thoCatToc.hoVaTen.asc())
				.fetch();
		return list;
	}
	
	public List<ThoCatToc> getListThoCatTocAndNull() {
		List<ThoCatToc> list = new ArrayList<ThoCatToc>();
		list.add(null);
		list.addAll(getListThoCatToc());
		return list;
	}
	
	public JPAQuery<ThoCatToc> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<ThoCatToc> phongBan = find(ThoCatToc.class)
				.where(QThoCatToc.thoCatToc.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QThoCatToc.thoCatToc.hoVaTen.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QThoCatToc.thoCatToc.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QThoCatToc.thoCatToc.ngaySua.desc());
		return phongBan;
	}
}
