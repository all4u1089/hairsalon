package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zul.Div;

import com.querydsl.jpa.impl.JPAQuery;
import vn.toancauxanh.gg.model.LichSuKhachHang;
import vn.toancauxanh.gg.model.QLichSuKhachHang;
import vn.toancauxanh.gg.model.ThoCatToc;
import vn.toancauxanh.service.BasicService;

public class ThongKeNhanVienService extends BasicService<Object>{
		
	private Date ngayBatDau;
	private Date ngayKetThuc;
	private ThoCatToc selectedThoCatToc;
	private List<Object[]> listObject = new ArrayList<Object[]>();
	private boolean flagThongKe;
	private long tongLamDuoc;
	
	public Date getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public Date getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	
	public List<Object[]> getListObject() {
		return listObject;
	}
	public void setListObject(List<Object[]> listObject) {
		this.listObject = listObject;
	}
	public boolean isFlagThongKe() {
		return flagThongKe;
	}
	public void setFlagThongKe(boolean flagThongKe) {
		this.flagThongKe = flagThongKe;
	}
	
	public ThoCatToc getSelectedThoCatToc() {
		return selectedThoCatToc;
	}
	public void setSelectedThoCatToc(ThoCatToc selectedThoCatToc) {
		this.selectedThoCatToc = selectedThoCatToc;
	}
	
	public long getTongLamDuoc() {
		return tongLamDuoc;
	}
	public void setTongLamDuoc(long tongLamDuoc) {
		this.tongLamDuoc = tongLamDuoc;
	}
	@Command
	public void thongKe(@BindingParam("div") final Div chartArea) {
		flagThongKe = true;
		BindUtils.postNotifyChange(null, null, this, "flagThongKe");
		BindUtils.postNotifyChange(null, null, this, "targetQuery");
	}
	
	public List<LichSuKhachHang> getTargetQuery() {
		List<LichSuKhachHang> list = new ArrayList<>();
		JPAQuery<LichSuKhachHang> q = find(LichSuKhachHang.class);
		if (selectedThoCatToc != null) {
			tongLamDuoc = 0;
			q.where(QLichSuKhachHang.lichSuKhachHang.chiTietDichVus.any().thoCatToc.eq(selectedThoCatToc));
			if (ngayBatDau != null && ngayKetThuc != null) {
				Calendar calKetThuc = Calendar.getInstance();
				calKetThuc.setTime(ngayKetThuc);
				calKetThuc.set(Calendar.HOUR, 16);
				calKetThuc.set(Calendar.MINUTE, 59);
				q.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.between(ngayBatDau, calKetThuc.getTime()));
			} else if (ngayBatDau != null) {
				q.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.after(ngayBatDau));
			} else if (ngayKetThuc != null) {
				Calendar calKetThuc = Calendar.getInstance();
				calKetThuc.setTime(ngayKetThuc);
				calKetThuc.set(Calendar.HOUR, 16);
				calKetThuc.set(Calendar.MINUTE, 59);
				q.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.before(calKetThuc.getTime()));
			}
			
			q.orderBy(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.desc());
			list = q.fetch();
			for (LichSuKhachHang lichSu : list) {
				tongLamDuoc += lichSu.getTongTienTheoNhanVien(selectedThoCatToc);
			}
			BindUtils.postNotifyChange(null, null, this, "tongLamDuoc");
		}
		return list;
	}
}
