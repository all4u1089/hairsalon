package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.GiaTienTayToc;
import vn.toancauxanh.gg.model.QGiaTienTayToc;
import vn.toancauxanh.service.BasicService;

public class GiaTienTayTocService extends BasicService<GiaTienTayToc>{
		
	@Init
	public void init() {
		List<GiaTienTayToc> list = find(GiaTienTayToc.class)
				.where(QGiaTienTayToc.giaTienTayToc.trangThai.ne(core().TT_DA_XOA))
				.fetch();
		if (list == null || list.isEmpty()) {
			bootstrapGiaTienTayToc();
		}
	}
	
	public List<GiaTienTayToc> getListGiaTienTayToc() {
		List<GiaTienTayToc> list = new ArrayList<GiaTienTayToc>();
		list = find(GiaTienTayToc.class)
				.where(QGiaTienTayToc.giaTienTayToc.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QGiaTienTayToc.giaTienTayToc.ten.asc())
				.fetch();
		return list;
	}
	
	public List<GiaTienTayToc> getListGiaTienTayTocAndNull() {
		List<GiaTienTayToc> list = new ArrayList<GiaTienTayToc>();
		list.add(null);
		list.addAll(getListGiaTienTayToc());
		return list;
	}
	
	public JPAQuery<GiaTienTayToc> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<GiaTienTayToc> phongBan = find(GiaTienTayToc.class)
				.where(QGiaTienTayToc.giaTienTayToc.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QGiaTienTayToc.giaTienTayToc.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QGiaTienTayToc.giaTienTayToc.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QGiaTienTayToc.giaTienTayToc.ngaySua.desc());
		return phongBan;
	}
	
	public void bootstrapGiaTienTayToc() {
		GiaTienTayToc giaTien = new GiaTienTayToc();
		giaTien.setTen("Ít");
		giaTien.setGiaTien(250000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienTayToc();
		giaTien.setTen("Nhiều");
		giaTien.setGiaTien(300000);
		giaTien.saveNotShowNotification();
	}
}
