package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;
import vn.toancauxanh.gg.model.GiaTienUonDuoiNhuomTocNam;
import vn.toancauxanh.gg.model.QGiaTienUonDuoiNhuomTocNam;
import vn.toancauxanh.service.BasicService;

public class GiaTienUonDuoiNhuomTocNamService extends BasicService<GiaTienUonDuoiNhuomTocNam>{
		
	@Init
	public void init() {
		List<GiaTienUonDuoiNhuomTocNam> list = find(GiaTienUonDuoiNhuomTocNam.class)
				.where(QGiaTienUonDuoiNhuomTocNam.giaTienUonDuoiNhuomTocNam.trangThai.ne(core().TT_DA_XOA))
				.fetch();
		if (list == null || list.isEmpty()) {
			bootstrapGiaTienCatToc();
		}
	}
	
	public List<GiaTienUonDuoiNhuomTocNam> getListGiaTienUonDuoiNhuomTocNam() {
		List<GiaTienUonDuoiNhuomTocNam> list = new ArrayList<GiaTienUonDuoiNhuomTocNam>();
		list = find(GiaTienUonDuoiNhuomTocNam.class)
				.where(QGiaTienUonDuoiNhuomTocNam.giaTienUonDuoiNhuomTocNam.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QGiaTienUonDuoiNhuomTocNam.giaTienUonDuoiNhuomTocNam.ten.asc())
				.fetch();
		return list;
	}
	
	public List<GiaTienUonDuoiNhuomTocNam> getListGiaTienUonDuoiNhuomTocNamAndNull() {
		List<GiaTienUonDuoiNhuomTocNam> list = new ArrayList<GiaTienUonDuoiNhuomTocNam>();
		list.add(null);
		list.addAll(getListGiaTienUonDuoiNhuomTocNam());
		return list;
	}
	
	public JPAQuery<GiaTienUonDuoiNhuomTocNam> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<GiaTienUonDuoiNhuomTocNam> phongBan = find(GiaTienUonDuoiNhuomTocNam.class)
				.where(QGiaTienUonDuoiNhuomTocNam.giaTienUonDuoiNhuomTocNam.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QGiaTienUonDuoiNhuomTocNam.giaTienUonDuoiNhuomTocNam.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QGiaTienUonDuoiNhuomTocNam.giaTienUonDuoiNhuomTocNam.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QGiaTienUonDuoiNhuomTocNam.giaTienUonDuoiNhuomTocNam.ngaySua.desc());
		return phongBan;
	}
	
	public void bootstrapGiaTienCatToc() {
		GiaTienUonDuoiNhuomTocNam giaTien = new GiaTienUonDuoiNhuomTocNam();
		giaTien.setTen("Ít");
		giaTien.setGiaTien(250000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienUonDuoiNhuomTocNam();
		giaTien.setTen("Nhiều");
		giaTien.setGiaTien(300000);
		giaTien.saveNotShowNotification();
	}
}
