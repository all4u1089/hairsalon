package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.GiaTienPhucHoi;
import vn.toancauxanh.gg.model.QGiaTienPhucHoi;
import vn.toancauxanh.service.BasicService;

public class GiaTienPhucHoiService extends BasicService<GiaTienPhucHoi>{
		
	@Init
	public void init() {
		List<GiaTienPhucHoi> list = find(GiaTienPhucHoi.class)
				.where(QGiaTienPhucHoi.giaTienPhucHoi.trangThai.ne(core().TT_DA_XOA))
				.fetch();
		if (list == null || list.isEmpty()) {
			bootstrapGiaTienPhucHoi();
		}
	}
	
	public List<GiaTienPhucHoi> getListGiaTienPhucHoi() {
		List<GiaTienPhucHoi> list = new ArrayList<GiaTienPhucHoi>();
		list = find(GiaTienPhucHoi.class)
				.where(QGiaTienPhucHoi.giaTienPhucHoi.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QGiaTienPhucHoi.giaTienPhucHoi.ten.asc())
				.fetch();
		return list;
	}
	
	public List<GiaTienPhucHoi> getListGiaTienPhucHoiAndNull() {
		List<GiaTienPhucHoi> list = new ArrayList<GiaTienPhucHoi>();
		list.add(null);
		list.addAll(getListGiaTienPhucHoi());
		return list;
	}
	
	public JPAQuery<GiaTienPhucHoi> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<GiaTienPhucHoi> phongBan = find(GiaTienPhucHoi.class)
				.where(QGiaTienPhucHoi.giaTienPhucHoi.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QGiaTienPhucHoi.giaTienPhucHoi.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QGiaTienPhucHoi.giaTienPhucHoi.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QGiaTienPhucHoi.giaTienPhucHoi.ngaySua.desc());
		return phongBan;
	}
	
	public void bootstrapGiaTienPhucHoi() {
		GiaTienPhucHoi giaTien = new GiaTienPhucHoi();
		giaTien.setTen("Dài");
		giaTien.setGiaTien(450000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienPhucHoi();
		giaTien.setTen("Ngắn");
		giaTien.setGiaTien(300000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienPhucHoi();
		giaTien.setTen("Vừa");
		giaTien.setGiaTien(350000);
		giaTien.saveNotShowNotification();
	}
}
