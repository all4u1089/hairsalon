package vn.toancauxanh.cms.service;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.ChiPhi;
import vn.toancauxanh.gg.model.LoaiChiTieu;
import vn.toancauxanh.gg.model.QChiPhi;
import vn.toancauxanh.service.BasicService;

public class ChiPhiService extends BasicService<ChiPhi>{
	
	private LoaiChiTieu selectedLoaiChiTieu;
	
	public LoaiChiTieu getSelectedLoaiChiTieu() {
		return selectedLoaiChiTieu;
	}

	public void setSelectedLoaiChiTieu(LoaiChiTieu selectedLoaiChiTieu) {
		this.selectedLoaiChiTieu = selectedLoaiChiTieu;
	}

	public JPAQuery<ChiPhi> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();

		JPAQuery<ChiPhi> q = find(ChiPhi.class)
				.where(QChiPhi.chiPhi.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			q.where(QChiPhi.chiPhi.ten.like(tukhoa));	
		}
		
		if (selectedLoaiChiTieu != null) {
			q.where(QChiPhi.chiPhi.loaiChiTieu.eq(selectedLoaiChiTieu));
		}
		
		q.orderBy(QChiPhi.chiPhi.ngayPhatSinh.desc());
		return q;
	}
}
