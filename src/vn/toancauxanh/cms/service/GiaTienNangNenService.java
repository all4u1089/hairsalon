package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.GiaTienNangNen;
import vn.toancauxanh.gg.model.QGiaTienNangNen;
import vn.toancauxanh.service.BasicService;

public class GiaTienNangNenService extends BasicService<GiaTienNangNen>{
		
	@Init
	public void init() {
		List<GiaTienNangNen> list = find(GiaTienNangNen.class)
				.where(QGiaTienNangNen.giaTienNangNen.trangThai.ne(core().TT_DA_XOA))
				.fetch();
		if (list == null || list.isEmpty()) {
			bootstrapGiaTienNangNen();
		}
	}
	
	public List<GiaTienNangNen> getListGiaTienNangNen() {
		List<GiaTienNangNen> list = new ArrayList<GiaTienNangNen>();
		list = find(GiaTienNangNen.class)
				.where(QGiaTienNangNen.giaTienNangNen.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QGiaTienNangNen.giaTienNangNen.ten.asc())
				.fetch();
		return list;
	}
	
	public List<GiaTienNangNen> getListGiaTienNangNenAndNull() {
		List<GiaTienNangNen> list = new ArrayList<GiaTienNangNen>();
		list.add(null);
		list.addAll(getListGiaTienNangNen());
		return list;
	}
	
	public JPAQuery<GiaTienNangNen> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<GiaTienNangNen> phongBan = find(GiaTienNangNen.class)
				.where(QGiaTienNangNen.giaTienNangNen.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QGiaTienNangNen.giaTienNangNen.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QGiaTienNangNen.giaTienNangNen.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QGiaTienNangNen.giaTienNangNen.ngaySua.desc());
		return phongBan;
	}
	
	public void bootstrapGiaTienNangNen() {
		GiaTienNangNen giaTien = new GiaTienNangNen();
		giaTien.setTen("Ít");
		giaTien.setGiaTien(250000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienNangNen();
		giaTien.setTen("Nhiều");
		giaTien.setGiaTien(300000);
		giaTien.saveNotShowNotification();
	}
}
