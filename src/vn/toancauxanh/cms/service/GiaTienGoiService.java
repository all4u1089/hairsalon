package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.GiaTienGoi;
import vn.toancauxanh.gg.model.QGiaTienGoi;
import vn.toancauxanh.service.BasicService;

public class GiaTienGoiService extends BasicService<GiaTienGoi>{
		
	@Init
	public void init() {
		List<GiaTienGoi> list = find(GiaTienGoi.class)
				.where(QGiaTienGoi.giaTienGoi.trangThai.ne(core().TT_DA_XOA))
				.fetch();
		if (list == null || list.isEmpty()) {
			bootstrapGiaTienGoi();
		}
	}
	
	public List<GiaTienGoi> getListGiaTienGoi() {
		List<GiaTienGoi> list = new ArrayList<GiaTienGoi>();
		list = find(GiaTienGoi.class)
				.where(QGiaTienGoi.giaTienGoi.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QGiaTienGoi.giaTienGoi.ten.asc())
				.fetch();
		return list;
	}
	
	public List<GiaTienGoi> getListGiaTienGoiAndNull() {
		List<GiaTienGoi> list = new ArrayList<GiaTienGoi>();
		list.add(null);
		list.addAll(getListGiaTienGoi());
		return list;
	}
	
	public JPAQuery<GiaTienGoi> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<GiaTienGoi> phongBan = find(GiaTienGoi.class)
				.where(QGiaTienGoi.giaTienGoi.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QGiaTienGoi.giaTienGoi.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QGiaTienGoi.giaTienGoi.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QGiaTienGoi.giaTienGoi.ngaySua.desc());
		return phongBan;
	}
	
	public void bootstrapGiaTienGoi() {
		GiaTienGoi giaTien = new GiaTienGoi();
		giaTien.setTen("Nhanh");
		giaTien.setGiaTien(20000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienGoi();
		giaTien.setTen("Vừa");
		giaTien.setGiaTien(50000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienGoi();
		giaTien.setTen("Tốt");
		giaTien.setGiaTien(80000);
		giaTien.saveNotShowNotification();
	}
}
