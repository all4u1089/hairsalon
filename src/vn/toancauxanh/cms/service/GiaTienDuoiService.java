package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.GiaTienCatToc;
import vn.toancauxanh.gg.model.GiaTienDuoi;
import vn.toancauxanh.gg.model.QGiaTienCatToc;
import vn.toancauxanh.gg.model.QGiaTienDuoi;
import vn.toancauxanh.service.BasicService;

public class GiaTienDuoiService extends BasicService<GiaTienDuoi>{
		
	@Init
	public void init() {
		List<GiaTienDuoi> list = find(GiaTienDuoi.class)
				.where(QGiaTienDuoi.giaTienDuoi.trangThai.ne(core().TT_DA_XOA))
				.fetch();
		if (list == null || list.isEmpty()) {
			bootstrapGiaTienDuoi();
		}
	}
	
	public List<GiaTienDuoi> getListGiaTienDuoi() {
		List<GiaTienDuoi> list = new ArrayList<GiaTienDuoi>();
		list = find(GiaTienDuoi.class)
				.where(QGiaTienDuoi.giaTienDuoi.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QGiaTienDuoi.giaTienDuoi.ten.asc())
				.fetch();
		return list;
	}
	
	public List<GiaTienDuoi> getListGiaTienDuoiAndNull() {
		List<GiaTienDuoi> list = new ArrayList<GiaTienDuoi>();
		list.add(null);
		list.addAll(getListGiaTienDuoi());
		return list;
	}
	
	public JPAQuery<GiaTienDuoi> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<GiaTienDuoi> phongBan = find(GiaTienDuoi.class)
				.where(QGiaTienDuoi.giaTienDuoi.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QGiaTienDuoi.giaTienDuoi.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QGiaTienDuoi.giaTienDuoi.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QGiaTienDuoi.giaTienDuoi.ngaySua.desc());
		return phongBan;
	}
	
	public void bootstrapGiaTienDuoi() {
		GiaTienDuoi giaTien = new GiaTienDuoi();
		giaTien.setTen("Ít");
		giaTien.setGiaTien(250000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienDuoi();
		giaTien.setTen("Nhiều");
		giaTien.setGiaTien(300000);
		giaTien.saveNotShowNotification();
	}
}
