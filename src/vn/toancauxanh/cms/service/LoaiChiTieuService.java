package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.LoaiChiTieu;
import vn.toancauxanh.gg.model.QLoaiChiTieu;
import vn.toancauxanh.service.BasicService;

public class LoaiChiTieuService extends BasicService<LoaiChiTieu>{
		
	public List<LoaiChiTieu> getListLoaiChiTieu() {
		List<LoaiChiTieu> list = new ArrayList<LoaiChiTieu>();
		list = find(LoaiChiTieu.class)
				.where(QLoaiChiTieu.loaiChiTieu.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QLoaiChiTieu.loaiChiTieu.ten.asc())
				.fetch();
		return list;
	}
	
	public List<LoaiChiTieu> getListLoaiChiTieuAndNull() {
		List<LoaiChiTieu> list = new ArrayList<LoaiChiTieu>();
		list.add(null);
		list.addAll(getListLoaiChiTieu());
		return list;
	}
	
	public JPAQuery<LoaiChiTieu> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<LoaiChiTieu> phongBan = find(LoaiChiTieu.class)
				.where(QLoaiChiTieu.loaiChiTieu.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QLoaiChiTieu.loaiChiTieu.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QLoaiChiTieu.loaiChiTieu.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QLoaiChiTieu.loaiChiTieu.ngaySua.desc());
		return phongBan;
	}
}
