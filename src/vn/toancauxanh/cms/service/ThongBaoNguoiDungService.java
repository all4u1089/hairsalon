package vn.toancauxanh.cms.service;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.GioiTinh;
import vn.toancauxanh.gg.model.QThongBaoNguoiDung;
import vn.toancauxanh.gg.model.ThongBaoNguoiDung;
import vn.toancauxanh.service.BasicService;

public class ThongBaoNguoiDungService extends BasicService<GioiTinh>{
			
	public JPAQuery<ThongBaoNguoiDung> getTargetQuery() {

		JPAQuery<ThongBaoNguoiDung> thongBao = find(ThongBaoNguoiDung.class)
				.where(QThongBaoNguoiDung.thongBaoNguoiDung.trangThai.ne(core().TT_DA_XOA));
		if (core().getNhanVien() != null) {
			thongBao.where(QThongBaoNguoiDung.thongBaoNguoiDung.canBo.eq(core().getNhanVien()));
		}
		thongBao.orderBy(QThongBaoNguoiDung.thongBaoNguoiDung.ngayTao.desc());
		return thongBao;
	}
}
