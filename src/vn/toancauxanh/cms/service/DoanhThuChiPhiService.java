package vn.toancauxanh.cms.service;
import java.util.Calendar;
import java.util.Date;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;

import vn.toancauxanh.gg.model.ChiPhi;
import vn.toancauxanh.gg.model.LichSuKhachHang;
import vn.toancauxanh.gg.model.QChiPhi;
import vn.toancauxanh.gg.model.QLichSuKhachHang;
import vn.toancauxanh.service.BasicService;

public class DoanhThuChiPhiService extends BasicService<Object>{
		
	private Date ngayBatDau;
	private Date ngayKetThuc;
	
	private long doanhThu;
	private long chiPhi;
	private long loiNhuan;
	
	public Date getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public Date getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	
	public long getDoanhThu() {
		return doanhThu;
	}
	public void setDoanhThu(long doanhThu) {
		this.doanhThu = doanhThu;
	}
	public long getChiPhi() {
		return chiPhi;
	}
	public void setChiPhi(long chiPhi) {
		this.chiPhi = chiPhi;
	}
	public long getLoiNhuan() {
		return loiNhuan;
	}
	public void setLoiNhuan(long loiNhuan) {
		this.loiNhuan = loiNhuan;
	}
	@Command
	public void thongKe() {
		
		if (ngayBatDau != null && ngayKetThuc != null) {
			Calendar calKetThuc = Calendar.getInstance();
			calKetThuc.setTime(ngayKetThuc);
			calKetThuc.set(Calendar.HOUR, 16);
			calKetThuc.set(Calendar.MINUTE, 59);
			try {
				doanhThu = find(LichSuKhachHang.class)
						.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.between(ngayBatDau, calKetThuc.getTime()))
						.select(QLichSuKhachHang.lichSuKhachHang.tongTienThanhToan.sum()).fetchFirst();				
			} catch (Exception ex) {
				doanhThu = 0;				
			}
			try {
				chiPhi = find(ChiPhi.class)
						.where(QChiPhi.chiPhi.ngayPhatSinh.between(ngayBatDau, calKetThuc.getTime()))
						.select(QChiPhi.chiPhi.giaTien.sum()).fetchFirst();
			} catch (Exception ex) {
				chiPhi = 0;
			}
			loiNhuan = doanhThu - chiPhi;
		} else if (ngayBatDau != null) {
			try {
				doanhThu = find(LichSuKhachHang.class)
						.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.after(ngayBatDau))
						.select(QLichSuKhachHang.lichSuKhachHang.tongTienThanhToan.sum()).fetchFirst();				
			} catch (Exception ex) {
				doanhThu = 0;				
			}
			try {
				chiPhi = find(ChiPhi.class)
						.where(QChiPhi.chiPhi.ngayPhatSinh.after(ngayBatDau))
						.select(QChiPhi.chiPhi.giaTien.sum()).fetchFirst();
			} catch (Exception ex) {
				chiPhi = 0;
			}
			loiNhuan = doanhThu - chiPhi;
		} else if (ngayKetThuc != null) {
			Calendar calKetThuc = Calendar.getInstance();
			calKetThuc.setTime(ngayKetThuc);
			calKetThuc.set(Calendar.HOUR, 16);
			calKetThuc.set(Calendar.MINUTE, 59);
			try {
				doanhThu = find(LichSuKhachHang.class)
						.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.before(calKetThuc.getTime()))
						.select(QLichSuKhachHang.lichSuKhachHang.tongTienThanhToan.sum()).fetchFirst();				
			} catch (Exception ex) {
				doanhThu = 0;				
			}
			try {
				chiPhi = find(ChiPhi.class)
						.where(QChiPhi.chiPhi.ngayPhatSinh.before(calKetThuc.getTime()))
						.select(QChiPhi.chiPhi.giaTien.sum()).fetchFirst();
			} catch (Exception ex) {
				chiPhi = 0;
			}
			loiNhuan = doanhThu - chiPhi;
		} else {
			try {
				doanhThu = find(LichSuKhachHang.class)
						.select(QLichSuKhachHang.lichSuKhachHang.tongTienThanhToan.sum()).fetchFirst();				
			} catch (Exception ex) {
				doanhThu = 0;				
			}
			try {
				chiPhi = find(ChiPhi.class)
						.select(QChiPhi.chiPhi.giaTien.sum()).fetchFirst();
			} catch (Exception ex) {
				chiPhi = 0;
			}
			loiNhuan = doanhThu - chiPhi;
		}
		BindUtils.postNotifyChange(null, null, this, "doanhThu");
		BindUtils.postNotifyChange(null, null, this, "chiPhi");
		BindUtils.postNotifyChange(null, null, this, "loiNhuan");
	}
}
