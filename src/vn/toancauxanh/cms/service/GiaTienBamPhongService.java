package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.GiaTienBamPhong;
import vn.toancauxanh.gg.model.QGiaTienBamPhong;
import vn.toancauxanh.service.BasicService;

public class GiaTienBamPhongService extends BasicService<GiaTienBamPhong>{
		
	@Init
	public void init() {
		List<GiaTienBamPhong> list = find(GiaTienBamPhong.class)
				.where(QGiaTienBamPhong.giaTienBamPhong.trangThai.ne(core().TT_DA_XOA))
				.fetch();
		if (list == null || list.isEmpty()) {
			bootstrapGiaTienBamPhong();
		}
	}
	
	public List<GiaTienBamPhong> getListGiaTienBamPhong() {
		List<GiaTienBamPhong> list = new ArrayList<GiaTienBamPhong>();
		list = find(GiaTienBamPhong.class)
				.where(QGiaTienBamPhong.giaTienBamPhong.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QGiaTienBamPhong.giaTienBamPhong.ten.asc())
				.fetch();
		return list;
	}
	
	public List<GiaTienBamPhong> getListGiaTienBamPhongAndNull() {
		List<GiaTienBamPhong> list = new ArrayList<GiaTienBamPhong>();
		list.add(null);
		list.addAll(getListGiaTienBamPhong());
		return list;
	}
	
	public JPAQuery<GiaTienBamPhong> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<GiaTienBamPhong> phongBan = find(GiaTienBamPhong.class)
				.where(QGiaTienBamPhong.giaTienBamPhong.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QGiaTienBamPhong.giaTienBamPhong.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QGiaTienBamPhong.giaTienBamPhong.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QGiaTienBamPhong.giaTienBamPhong.ngaySua.desc());
		return phongBan;
	}
	
	public void bootstrapGiaTienBamPhong() {
		GiaTienBamPhong giaTien = new GiaTienBamPhong();
		giaTien.setTen("Ít");
		giaTien.setGiaTien(250000);
		giaTien.saveNotShowNotification();
		
		giaTien = new GiaTienBamPhong();
		giaTien.setTen("Nhiều");
		giaTien.setGiaTien(300000);
		giaTien.saveNotShowNotification();
	}
}
