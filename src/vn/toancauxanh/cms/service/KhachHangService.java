package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.KhachHang;
import vn.toancauxanh.gg.model.LichSuKhachHang;
import vn.toancauxanh.gg.model.QChiPhi;
import vn.toancauxanh.gg.model.QKhachHang;
import vn.toancauxanh.gg.model.QLichSuKhachHang;
import vn.toancauxanh.gg.model.ChiPhi;
import vn.toancauxanh.gg.model.GioiTinh;
import vn.toancauxanh.service.BasicService;

public class KhachHangService extends BasicService<KhachHang>{
	
	@Init
	public void init() {
		int pageSize = getCookie();
		if (pageSize == 0) {
			setCookie(10);
			pageSize = 10;
		}
		this.argDeco().put("pagesize", pageSize);
	}
	private GioiTinh selectedGioiTinh;
	private Date ngaySinh;
	private Date ngaySuDung;
	private Long count;
	private long countDoanhThu;
	private long countChiPhi;
	private List<Integer> listSoMucTrenTrang = new ArrayList<Integer>(Arrays.asList(5, 10, 20, 30, 50, 75, 100));

	public List<Integer> getListSoMucTrenTrang() {
		return listSoMucTrenTrang;
	}

	public void setListSoMucTrenTrang(List<Integer> listSoMucTrenTrang) {
		this.listSoMucTrenTrang = listSoMucTrenTrang;
	}
	
	public GioiTinh getSelectedGioiTinh() {
		return selectedGioiTinh;
	}

	public void setSelectedGioiTinh(GioiTinh selectedGioiTinh) {
		this.selectedGioiTinh = selectedGioiTinh;
	}	
	
	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Date getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(Date ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	
	public Date getNgaySuDung() {
		if (ngaySuDung == null) {
			ngaySuDung = new Date();
		}
		return ngaySuDung;
	}

	public void setNgaySuDung(Date ngaySuDung) {
		this.ngaySuDung = ngaySuDung;
	}
	
	public long getCountDoanhThu() {
		return countDoanhThu;
	}

	public void setCountDoanhThu(long countDoanhThu) {
		this.countDoanhThu = countDoanhThu;
	}

	public long getCountChiPhi() {
		return countChiPhi;
	}

	public void setCountChiPhi(long countChiPhi) {
		this.countChiPhi = countChiPhi;
	}

	public OrderSpecifier<Date> getOrderBy() {
		return QKhachHang.khachHang.ngaySua.desc();
	}
	
	public JPAQuery<KhachHang> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();

		JPAQuery<KhachHang> q = find(KhachHang.class)
				.where(QKhachHang.khachHang.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			q.where(QKhachHang.khachHang.hoVaTen.containsIgnoreCase(tuKhoa)
					.or(QKhachHang.khachHang.tenFacebook.containsIgnoreCase(tuKhoa))
					.or(QKhachHang.khachHang.soDienThoai.containsIgnoreCase(tuKhoa))
					.or(QKhachHang.khachHang.dacDiemNhanDang.containsIgnoreCase(tuKhoa))
					.or(QKhachHang.khachHang.soCMND.contains(tuKhoa)));	
		}
		count = q.fetchCount();
		BindUtils.postNotifyChange(null, null, this, "count");
		return q;
	}
	
	public List<KhachHang> getListSinhNhat() {
		List<KhachHang> list = new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		list = find(KhachHang.class)
				.where(QKhachHang.khachHang.ngaySinh.dayOfYear().goe(cal.get(Calendar.DAY_OF_YEAR)))
				.orderBy(QKhachHang.khachHang.ngaySinh.dayOfYear().asc())
				.limit(10).fetch();
		return list;
	}
	
	public List<LichSuKhachHang> getListChamSoc() {
		List<LichSuKhachHang> list = new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		list = find(LichSuKhachHang.class)
				.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.after(cal.getTime())
						.or(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.eq(cal.getTime())))
				.orderBy(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.asc())
				.limit(10)
				.fetch();
		return list;
	}
	
	public List<LichSuKhachHang> getListDichVuTrongNgay() {
		List<LichSuKhachHang> list = new ArrayList<>();
		if (ngaySuDung != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(ngaySuDung);
			list = find(LichSuKhachHang.class)
					.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.year().eq(cal.get(Calendar.YEAR)))
					.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.month().eq(cal.get(Calendar.MONTH) + 1))
					.where(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.dayOfMonth().eq(cal.get(Calendar.DAY_OF_MONTH)))
					.orderBy(QLichSuKhachHang.lichSuKhachHang.ngaySuDung.asc())
					.fetch();
			countDoanhThu = 0;
			for (LichSuKhachHang lichSu : list) {
				countDoanhThu += lichSu.getTongTienThanhToan();
			}
		}
		
		return list;
	}
	
	public List<ChiPhi> getListChiTieuTrongNgay() {
		List<ChiPhi> list = new ArrayList<>();
		if (ngaySuDung != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(ngaySuDung);
			list = find(ChiPhi.class)
					.where(QChiPhi.chiPhi.ngayPhatSinh.year().eq(cal.get(Calendar.YEAR)))
					.where(QChiPhi.chiPhi.ngayPhatSinh.month().eq(cal.get(Calendar.MONTH) + 1))
					.where(QChiPhi.chiPhi.ngayPhatSinh.dayOfMonth().eq(cal.get(Calendar.DAY_OF_MONTH)))
					.orderBy(QChiPhi.chiPhi.ngayPhatSinh.asc())
					.fetch();
			countChiPhi = 0;
			for (ChiPhi lichSu : list) {
				countChiPhi += lichSu.getGiaTien();
			}
		}
		return list;
	}
	
	@Command
	public void reloadDanhSachTrongNgay() {
		BindUtils.postNotifyChange(null, null, this, "listDichVuTrongNgay");
		BindUtils.postNotifyChange(null, null, this, "listChiTieuTrongNgay");
		BindUtils.postNotifyChange(null, null, this, "countDoanhThu");
		BindUtils.postNotifyChange(null, null, this, "countChiPhi");
	}
	
	@Command
	public void onShowTDP() {
		BindUtils.postNotifyChange(null, null, this, "tenToDanPho");
	}
	
	
	public int getCookie() {
		HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
		int pageSize = 0;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie c : cookies) {
				if ("pageSize".equals(c.getName())) {
					pageSize = Integer.parseInt(c.getValue());
					break;
				}
			}
		}
		return pageSize;
	}
	
	public void setCookie(int pageSize) {
		HttpServletResponse response = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
		Cookie userCookie = new Cookie("pageSize", pageSize + ""); 
		response.addCookie(userCookie); 
	}
}
