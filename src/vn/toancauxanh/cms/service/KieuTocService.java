package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapUtils;
import org.zkoss.util.resource.Labels;

import com.querydsl.jpa.impl.JPAQuery;
import vn.toancauxanh.gg.model.KieuToc;
import vn.toancauxanh.gg.model.QKieuToc;
import vn.toancauxanh.service.BasicService;

public class KieuTocService extends BasicService<KieuToc>{
		
	public List<KieuToc> getListKieuToc() {
		List<KieuToc> list = new ArrayList<KieuToc>();
		list = find(KieuToc.class)
				.where(QKieuToc.kieuToc.trangThai.eq(core().TT_AP_DUNG))
				.orderBy(QKieuToc.kieuToc.ten.asc())
				.fetch();
		return list;
	}
	
	public List<KieuToc> getListKieuTocAndNull() {
		List<KieuToc> list = new ArrayList<KieuToc>();
		list.add(null);
		list.addAll(getListKieuToc());
		return list;
	}
	
	public JPAQuery<KieuToc> getTargetQuery() {
		String tuKhoa = MapUtils.getString(argDeco(), Labels.getLabel("param.tukhoa"),"").trim();
		String trangThai = MapUtils.getString(argDeco(), Labels.getLabel("param.trangthai"), "");

		JPAQuery<KieuToc> phongBan = find(KieuToc.class)
				.where(QKieuToc.kieuToc.trangThai.ne(core().TT_DA_XOA));

		if (tuKhoa != null && !tuKhoa.isEmpty()) {
			String tukhoa = "%" + tuKhoa + "%";
			phongBan.where(QKieuToc.kieuToc.ten.like(tukhoa));	
		}
		if (!trangThai.isEmpty()) {
			phongBan.where(QKieuToc.kieuToc.trangThai.eq(trangThai));
		}
		phongBan.orderBy(QKieuToc.kieuToc.ngaySua.desc());
		return phongBan;
	}
}
