package vn.toancauxanh.cms.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.google.charts.PieChart;
import org.zkoss.google.charts.data.DataTable;
import org.zkoss.zul.Div;

import vn.toancauxanh.gg.model.ChiTietDichVu;
import vn.toancauxanh.gg.model.QChiTietDichVu;
import vn.toancauxanh.gg.model.enums.LoaiDichVu;
import vn.toancauxanh.service.BasicService;

public class ThongKeDoanhThuService extends BasicService<Object>{
		
	private Date ngayBatDau;
	private Date ngayKetThuc;
	private List<Object[]> listObject = new ArrayList<Object[]>();
	private boolean flagThongKe;
	
	public Date getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public Date getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	
	public List<Object[]> getListObject() {
		return listObject;
	}
	public void setListObject(List<Object[]> listObject) {
		this.listObject = listObject;
	}
	public boolean isFlagThongKe() {
		return flagThongKe;
	}
	public void setFlagThongKe(boolean flagThongKe) {
		this.flagThongKe = flagThongKe;
	}
	@Command
	public void thongKe(@BindingParam("div") final Div chartArea) {
		flagThongKe = true;
		listObject.clear();
		if (ngayBatDau != null && ngayKetThuc != null) {
			Calendar calKetThuc = Calendar.getInstance();
			calKetThuc.setTime(ngayKetThuc);
			calKetThuc.set(Calendar.HOUR, 16);
			calKetThuc.set(Calendar.MINUTE, 59);
			long tongSo = 0;
			Object[] obj = null;
			for (LoaiDichVu dichVu : core().getListDichVu()) {
				obj = new Object[2];				
				obj[0] = dichVu.getText();
				if (!dichVu.equals(LoaiDichVu.BAN_HANG)) {
					long tongCoKhuyenMai = 0;
					try {
						tongCoKhuyenMai = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.between(ngayBatDau, calKetThuc.getTime()))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNotNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchCount();
						tongCoKhuyenMai = (long) (tongCoKhuyenMai*0.1);
					} catch (Exception ex) {}
					long tongKhongKhuyenMai = 0;
					try {
						tongKhongKhuyenMai = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai.eq(0L))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.between(ngayBatDau, calKetThuc.getTime()))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchFirst();
					} catch (Exception ex) {}
					long tongKhuyenMaiPhanTram = 0;
					try {
						tongKhuyenMaiPhanTram = (Long)find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.between(ngayBatDau, calKetThuc.getTime()))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai.gt(0L))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.subtract(
										((QChiTietDichVu.chiTietDichVu.giaTien
												.multiply(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai))
												.divide(100)))
										.sum().castToNum(Long.class))
								.fetchFirst();
					} catch (Exception e) {}	
					obj[1] = tongCoKhuyenMai + tongKhongKhuyenMai + tongKhuyenMaiPhanTram;
					tongSo += (long)obj[1];
				} else {
					long tongBanHang = 0;
					try {
						tongBanHang = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.between(ngayBatDau, calKetThuc.getTime()))
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchFirst();
					} catch (Exception ex) {}					
					obj[1] = tongBanHang;
					tongSo += (long)obj[1];
				}
				listObject.add(obj);
			}
			obj = new Object[2];
			obj[0] = "Tổng";
			obj[1] = tongSo;
			listObject.add(obj);
		} else if (ngayBatDau != null) {
			long tongSo = 0;
			Object[] obj = null;
			for (LoaiDichVu dichVu : core().getListDichVu()) {
				obj = new Object[2];				
				obj[0] = dichVu.getText();
				if (!dichVu.equals(LoaiDichVu.BAN_HANG)) {
					long tongCoKhuyenMai = 0;
					try {
						tongCoKhuyenMai = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.after(ngayBatDau))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNotNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchCount();
						tongCoKhuyenMai = (long) (tongCoKhuyenMai*0.1);
					} catch (Exception ex) {}
					long tongKhongKhuyenMai = 0;
					try {
						tongKhongKhuyenMai = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai.eq(0L))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.after(ngayBatDau))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchFirst();
					} catch (Exception ex) {}		
					long tongKhuyenMaiPhanTram = 0;
					try {
						tongKhuyenMaiPhanTram = (Long)find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.after(ngayBatDau))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai.gt(0L))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.subtract(
										((QChiTietDichVu.chiTietDichVu.giaTien
												.multiply(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai))
												.divide(100)))
										.sum().castToNum(Long.class))
								.fetchFirst();
					} catch (Exception e) {}	
					obj[1] = tongCoKhuyenMai + tongKhongKhuyenMai + tongKhuyenMaiPhanTram;
					tongSo += (long)obj[1];
				} else {
					long tongBanHang = 0;
					try {
						tongBanHang = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.after(ngayBatDau))
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchFirst();
					} catch (Exception ex) {}					
					obj[1] = tongBanHang;
					tongSo += (long)obj[1];
				}
				listObject.add(obj);
			}
			obj = new Object[2];
			obj[0] = "Tổng";
			obj[1] = tongSo;
			listObject.add(obj);
		} else if (ngayKetThuc != null) {
			Calendar calKetThuc = Calendar.getInstance();
			calKetThuc.setTime(ngayKetThuc);
			calKetThuc.set(Calendar.HOUR, 16);
			calKetThuc.set(Calendar.MINUTE, 59);
			long tongSo = 0;
			Object[] obj = null;
			for (LoaiDichVu dichVu : core().getListDichVu()) {
				obj = new Object[2];				
				obj[0] = dichVu.getText();
				if (!dichVu.equals(LoaiDichVu.BAN_HANG)) {
					long tongCoKhuyenMai = 0;
					try {
						tongCoKhuyenMai = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.before(calKetThuc.getTime()))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNotNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchCount();
						tongCoKhuyenMai = (long) (tongCoKhuyenMai*0.1);
					} catch (Exception ex) {}
					long tongKhongKhuyenMai = 0;
					try {
						tongKhongKhuyenMai = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai.eq(0L))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.before(calKetThuc.getTime()))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchFirst();
					} catch (Exception ex) {}	
					long tongKhuyenMaiPhanTram = 0;
					try {
						tongKhuyenMaiPhanTram = (Long)find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.before(calKetThuc.getTime()))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai.gt(0L))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.subtract(
										((QChiTietDichVu.chiTietDichVu.giaTien
												.multiply(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai))
												.divide(100)))
										.sum().castToNum(Long.class))
								.fetchFirst();
					} catch (Exception e) {}
					obj[1] = tongCoKhuyenMai + tongKhongKhuyenMai + tongKhuyenMaiPhanTram;
					tongSo += (long)obj[1];
				} else {
					long tongBanHang = 0;
					try {
						tongBanHang = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.ngaySuDung.before(calKetThuc.getTime()))
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchFirst();
					} catch (Exception ex) {}					
					obj[1] = tongBanHang;
					tongSo += (long)obj[1];
				}
				listObject.add(obj);
			}
			obj = new Object[2];
			obj[0] = "Tổng";
			obj[1] = tongSo;
			listObject.add(obj);
		} else {
			long tongSo = 0;
			Object[] obj = null;
			for (LoaiDichVu dichVu : core().getListDichVu()) {
				obj = new Object[2];				
				obj[0] = dichVu.getText();
				if (!dichVu.equals(LoaiDichVu.BAN_HANG)) {
					long tongCoKhuyenMai = 0;
					try {
						tongCoKhuyenMai = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNotNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchCount();
						tongCoKhuyenMai = (long) (tongCoKhuyenMai*0.1);
					} catch (Exception ex) {}
					long tongKhongKhuyenMai = 0;
					try {
						tongKhongKhuyenMai = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai.eq(0L))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchFirst();
					} catch (Exception ex) {}	
					long tongKhuyenMaiPhanTram = 0;
					try {
						tongKhuyenMaiPhanTram = (Long)find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai.gt(0L))
								.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.khachHangKhuyenMai.isNull())
								.select(QChiTietDichVu.chiTietDichVu.giaTien.subtract(
										((QChiTietDichVu.chiTietDichVu.giaTien
												.multiply(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.phanTramKhuyenMai))
												.divide(100)))
										.sum().castToNum(Long.class))
								.fetchFirst();
					} catch (Exception e) {}
					obj[1] = tongCoKhuyenMai + tongKhongKhuyenMai + tongKhuyenMaiPhanTram;
					tongSo += (long)obj[1];
				} else {
					long tongBanHang = 0;
					try {
						tongBanHang = find(ChiTietDichVu.class)
								.where(QChiTietDichVu.chiTietDichVu.loaiDichVu.eq(dichVu))
								.select(QChiTietDichVu.chiTietDichVu.giaTien.sum())
								.fetchFirst();
					} catch (Exception ex) {}					
					obj[1] = tongBanHang;
					tongSo += (long)obj[1];
				}
				listObject.add(obj);
			}
			obj = new Object[2];
			obj[0] = "Tổng";
			obj[1] = tongSo;
			listObject.add(obj);
		}
		showChart(chartArea);
		BindUtils.postNotifyChange(null, null, this, "listObject");
		BindUtils.postNotifyChange(null, null, this, "flagThongKe");
	}
	
	public void showChart(Div chartArea) {
		//System.out.println("okok: " + chartArea);
		DataTable data = new DataTable();
		data.addStringColumn("Task", "task");
		data.addNumberColumn("Hours per Day", "hours");
		for (Object[] obj : listObject) {
			if (!obj[0].equals("Tổng")) {
				data.addRow(obj[0], obj[1]);
			}			
		}
		Map<String, Object> area = new HashMap<String, Object>();
        area.put("width", "92%");
        area.put("height", "92%");
        area.put("left", "40");    
		PieChart chart = new PieChart();
		chart.setTitle("Nhân khẩu toàn thành phố");
		chart.setHeight("300px");
		chart.setOption("chartArea", area);
		chart.setData(data);
		chart.setOption("pieHole", 0.6);
		chartArea.appendChild(chart);
	}
}
