package vn.toancauxanh.model;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "settings")
public class Setting extends Model<Setting> {
	private String diaChiEmail = "";

	public String getDiaChiEmail() {
		return diaChiEmail;
	}

	public void setDiaChiEmail(String diaChiEmail) {
		this.diaChiEmail = diaChiEmail;
	}
}
