package vn.toancauxanh.gg.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.sys.ValidationMessages;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.model.Model;
@Entity
@Table(name = "khachhang", indexes = { @Index(columnList = "hoVaTen")})
public class KhachHang extends Model<KhachHang>{
	
	private String hoVaTen = "";
	private String tenFacebook = "";
	private GioiTinh gioiTinh;
	private Date ngaySinh;
	private int namSinh;
	private String dacDiemNhanDang ="";
	private String soCMND = "";
	private Image imageContent;
	private String iconName = "";
	private String iconUrl = "";
	private String soDienThoai = "";
	private String email = "";
	private String diaChiFacebook = "";
	private String diaChi = "";
	private KhachHang gioiThieu;
	private boolean daDungKMGioiThieu;
	
	@ManyToOne
	public KhachHang getGioiThieu() {
		return gioiThieu;
	}

	public void setGioiThieu(KhachHang gioiThieu) {
		this.gioiThieu = gioiThieu;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getHoVaTen() {
		return hoVaTen;
	}

	public void setHoVaTen(String hoVaTen) {
		this.hoVaTen = hoVaTen;
	}
	
	public int getNamSinh() {
		return namSinh;
	}

	public void setNamSinh(int namSinh) {
		this.namSinh = namSinh;
	}
	
	@ManyToOne
	public GioiTinh getGioiTinh() {
		return gioiTinh;
	}

	public void setGioiTinh(GioiTinh gioiTinh) {
		this.gioiTinh = gioiTinh;
	}

	public Date getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(Date ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	
	public String getDacDiemNhanDang() {
		return dacDiemNhanDang;
	}

	public void setDacDiemNhanDang(String dacDiemNhanDang) {
		this.dacDiemNhanDang = dacDiemNhanDang;
	}
	
	public String getTenFacebook() {
		return tenFacebook;
	}

	public void setTenFacebook(String tenFacebook) {
		this.tenFacebook = tenFacebook;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getSoCMND() {
		return soCMND;
	}

	public void setSoCMND(String soCMND) {
		this.soCMND = soCMND;
	}
	
	public String getSoDienThoai() {
		return soDienThoai;
	}

	public void setSoDienThoai(String sodienThoai) {
		this.soDienThoai = sodienThoai;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDiaChiFacebook() {
		return diaChiFacebook;
	}

	public void setDiaChiFacebook(String diaChiFacebook) {
		this.diaChiFacebook = diaChiFacebook;
	}
	
	public boolean isDaDungKMGioiThieu() {
		return daDungKMGioiThieu;
	}

	public void setDaDungKMGioiThieu(boolean daDungKMGioiThieu) {
		this.daDungKMGioiThieu = daDungKMGioiThieu;
	}



	@Transient
	private boolean flagImage = true;

	@Transient
	public org.zkoss.image.Image getImageContent() throws FileNotFoundException, IOException {
		if (imageContent == null && !core().TT_DA_XOA.equals(getTrangThai())) {
			if (flagImage) {

				loadImageIsView();
			}
		}
		return imageContent;
	}

	public void setImageContent(org.zkoss.image.Image _imageContent) {
		this.imageContent = _imageContent;
	}

	private void loadImageIsView() throws FileNotFoundException, IOException {
		String imgName = "";
		String path = "";
		path = folderStore() + getIconName();
		if (!"".equals(getIconUrl()) && new File(path).exists()) {
			try (FileInputStream fis = new FileInputStream(new File(path));) {
				setImageContent(new AImage(imgName, fis));
			}
		} else {
			String filePath = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/backend/assets/img/avatar.jpg");
			try (FileInputStream fis = new FileInputStream(new File(filePath));) {
				setImageContent(new AImage("imge", fis));
			}
		}
	}

	private boolean beforeSaveImg() throws IOException {
		if (getImageContent() == null) {
			return false;
		}
		saveImageToServer();
		return true;
	}
	
	@Command
	public void attachImages(@BindingParam("media") final Media media,
			@BindingParam("vmsgs") final ValidationMessages vmsgs) {
		
		if (media instanceof org.zkoss.image.Image) {
			String tenFile = media.getName();
			tenFile = tenFile.replace(" ", "");
			tenFile = unAccent(tenFile.substring(0, tenFile.lastIndexOf("."))) + "_"
					+ Calendar.getInstance().getTimeInMillis() + tenFile.substring(tenFile.lastIndexOf("."));
			setImageContent((org.zkoss.image.Image) media);
			setIconName(tenFile);
			System.out.println("attachImages: " + tenFile);
			if (vmsgs != null) {
				vmsgs.clearKeyMessages("errLabel");
			}
			BindUtils.postNotifyChange(null, null, this, "imageContent");
			BindUtils.postNotifyChange(null, null, this, "iconname");
		} else {
			showNotification("Không phải hình ảnh", "", "warning");
		}
	}

	protected void saveImageToServer() throws IOException {

		Image imageContent2 = getImageContent();
		if (imageContent2 != null) {
			// luu hinh
			if (getIconName() != null && !getIconName().isEmpty()) {
				setIconUrl(folderImageUrl().concat(getIconName()));
				final File baseDir = new File(folderStore().concat(getIconName()));
				Files.copy(baseDir, imageContent2.getStreamData());
			}
		}
	}

	@Transient
	public String folderImageUrl() {
		return "/" + Labels.getLabel("filestore.folder") + "/doituong/";
	}

	@Command
	public void deleteImg() {
		setImageContent(null);
		setIconName("");
		flagImage = false;
		BindUtils.postNotifyChange(null, null, this, "imageContent");
		BindUtils.postNotifyChange(null, null, this, "name");
	}
	
	@Command
	public void saveKhachHangMoi(@BindingParam("res") final String res) 
			throws IOException {	
		save();
		Executions.getCurrent().sendRedirect("/khachhang/xem/" + getId(), "");
	}

	@Command
	public void saveKhachHangDichVu(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) {
		System.out.println("saveKhachHangDichVu");
		save();
		wdn.detach();
		((LichSuKhachHang)listObject).setKhachHang(this);
		BindUtils.postNotifyChange(null, null, listObject, attr);
		BindUtils.postNotifyChange(null, null, listObject, "khachHang");
	}
	
	@Command
	public void saveKhachHang(@BindingParam("res") final String res) 
			throws IOException {
		save();
		Executions.getCurrent().sendRedirect("/" + res + "/xem/" + getId(), "");
	}
	
	@Command
	public void cancel(@BindingParam("res") final String res) 
			throws IOException {	
		Executions.sendRedirect("/" + res);
	}
	
	@Transient
	public List<LichSuKhachHang> getListLuotSuDungDichVu() {
		List<LichSuKhachHang> list = new ArrayList<LichSuKhachHang>();
		list = find(LichSuKhachHang.class)
				.where(QLichSuKhachHang.lichSuKhachHang.khachHang.eq(this))
				.fetch();
		return list;
	}
	
	@Transient
	public List<KhachHang> getListNguoiGioiThieu() {
		List<KhachHang> list = new ArrayList<>();
		list = find(KhachHang.class)
				.where(QKhachHang.khachHang.gioiThieu.eq(this))
				.fetch();
		return list;
	}
	
	private List<KhachHang> tacGiaTimKiems = new ArrayList<KhachHang>();
	
	@Transient
	public List<KhachHang> getTacGiaTimKiems() {
		if (tacGiaTimKiems.isEmpty()) {
			if (getGioiThieu() != null) {
				tacGiaTimKiems.addAll(find(KhachHang.class)
						.where(QKhachHang.khachHang.trangThai.eq(core().TT_AP_DUNG))
						.where(QKhachHang.khachHang.ne(getGioiThieu()))
						.fetch());
			} else {
				tacGiaTimKiems.addAll(find(KhachHang.class)
						.where(QKhachHang.khachHang.trangThai.eq(core().TT_AP_DUNG))
						.fetch());
			}			
		}
		return tacGiaTimKiems;
	}
	
	@Command
	public void timKiemTacGia(@BindingParam("tenTacGia") final String tenTacGia) {
		if (tenTacGia.isEmpty()) {
			tacGiaTimKiems.clear();
			tacGiaTimKiems.addAll(find(KhachHang.class)
					.where(QKhachHang.khachHang.trangThai.eq(core().TT_AP_DUNG))
					.fetch());
		} else {
			tacGiaTimKiems.clear();			
			JPAQuery<KhachHang> q = find(KhachHang.class)
					.where(QKhachHang.khachHang.trangThai.eq(core().TT_AP_DUNG))					
					.where(QKhachHang.khachHang.hoVaTen.containsIgnoreCase(tenTacGia)
							.or(QKhachHang.khachHang.tenFacebook.containsIgnoreCase(tenTacGia)));			
			
			if (q.fetchCount() > 0) {
				tacGiaTimKiems.addAll(q.orderBy(QKhachHang.khachHang.hoVaTen.asc()).fetch());
			} else {
				tacGiaTimKiems.add(null);
			}			
		}
		BindUtils.postNotifyChange(null, null, this, "tacGiaTimKiems");
	}
	
	@Command
	public void deleteKhachHang(@BindingParam("res") final String res) {
		
		if (!checkInUse()) {
			Messagebox.show("Việc lựa chọn xóa hồ sơ sẽ đồng nghĩa với việc mất hoàn toàn " +
					"dữ liệu của người này trong hệ thống." +
					" Bạn có chắc là muốn xóa hồ sơ \"" + getHoVaTen() + "\" không?", "BẠN MUỐN XÓA HỒ SƠ " + getHoVaTen() + "?", Messagebox.CANCEL | Messagebox.OK,
					Messagebox.QUESTION, new EventListener<Event>() {
						@Override
						public void onEvent(final Event event) {
							if (Messagebox.ON_OK.equals(event.getName())) {
								KhachHang.this.doDelete(true);
								List<LichSuKhachHang> list = find(LichSuKhachHang.class)
										.where(QLichSuKhachHang.lichSuKhachHang.trangThai.eq(core().TT_AP_DUNG))
										.where(QLichSuKhachHang.lichSuKhachHang.khachHang.eq(KhachHang.this))
										.fetch();
								for (LichSuKhachHang thongTin : list) {
									thongTin.doDelete(true);
									List<ChiTietDichVu> listChiTiet = find(ChiTietDichVu.class)
											.where(QChiTietDichVu.chiTietDichVu.trangThai.eq(core().TT_AP_DUNG))
											.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.eq(thongTin))
											.fetch();
									for (ChiTietDichVu chiTiet : listChiTiet) {
										chiTiet.doDelete(true);
									}
								}
								showNotification("Xóa thành công!", "", "success");
								Executions.sendRedirect("/" + res);
							}
						}
					});
		}
	}
}

