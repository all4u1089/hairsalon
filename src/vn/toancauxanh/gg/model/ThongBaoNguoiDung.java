package vn.toancauxanh.gg.model;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

import vn.toancauxanh.model.Model;
import vn.toancauxanh.model.NhanVien;

@Entity
@Table(name = "thongbaonguoidung")
public class ThongBaoNguoiDung extends Model<ThongBaoNguoiDung>{
	
	private KhachHang khachHang;
	private NhanVien canBo;
	private String thongBao;
	private boolean flagDaXem;
	
	@ManyToOne
	public KhachHang getKhachHang() {
		return khachHang;
	}

	public void setKhachHang(KhachHang khachHang) {
		this.khachHang = khachHang;
	}

	@ManyToOne
	public NhanVien getCanBo() {
		return canBo;
	}

	public void setCanBo(NhanVien canBo) {
		this.canBo = canBo;
	}

	public String getThongBao() {
		return thongBao;
	}	

	public void setThongBao(String thongBao) {
		this.thongBao = thongBao;
	}
	
	public boolean isFlagDaXem() {
		return flagDaXem;
	}
	
	public void setFlagDaXem(boolean flagDaXem) {
		this.flagDaXem = flagDaXem;
	}

	@Command
	public void saveThongBaoNguoiDung(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {	
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, attr);		
	}
	
	@Transient
	public String getThoiGian() {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return df.format(getNgayTao());
	}
	
	@Command
	public void clickThongBao(@BindingParam("thongBao") final ThongBaoNguoiDung thongBao) {
		thongBao.setFlagDaXem(true);
		thongBao.saveNotShowNotification();
		Executions.sendRedirect("/khachhang/xem/" + thongBao.getKhachHang().getId());
	}
}
