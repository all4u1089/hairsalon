package vn.toancauxanh.gg.model;

import java.io.IOException;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.querydsl.core.annotations.QueryInit;

import vn.toancauxanh.gg.model.enums.DoDaiToc;
import vn.toancauxanh.gg.model.enums.DoDayToc;
import vn.toancauxanh.gg.model.enums.LoaiDichVu;
import vn.toancauxanh.gg.model.enums.LoaiThuoc;
import vn.toancauxanh.model.Model;

@Entity
@Table(name = "chitietdichvu")
public class ChiTietDichVu extends Model<ChiTietDichVu>{

	private LichSuKhachHang lichSuKhachHang;
	private MaMau maMau;
	private KieuToc kieuToc;
	private GiaTienCatToc giaTienCatToc;
	private GiaTienPhucHoi giaTienPhucHoi;
	private GiaTienGoi giaTienGoi;
	private GiaTienDuoi giaTienDuoi;
	private GiaTienBamPhong giaTienBamPhong;
	private GiaTienUonDuoiNhuomTocNam giaTienUonDuoiNhuomTocNam;
	private GiaTienNangNen giaTienNangNen;
	private GiaTienTayToc giaTienTayToc;
	private LoaiDichVu loaiDichVu;
	private LoaiThuoc loaiThuoc;
	private DoDaiToc doDaiToc;
	private DoDayToc doDayToc;
	private String truc = "";
	private String nhiet = "";
	private String oxy = "";
	private String tenSanPhamBanHang = "";
	private long giaTien;
	private ThoCatToc thoCatToc;
	
	public long getGiaTien() {
		return giaTien;
	}

	public void setGiaTien(long giaTien) {
		this.giaTien = giaTien;
	}

	public String getTenSanPhamBanHang() {
		return tenSanPhamBanHang;
	}

	public void setTenSanPhamBanHang(String tenSanPhamBanHang) {
		this.tenSanPhamBanHang = tenSanPhamBanHang;
	}
	
	@ManyToOne
	public GiaTienTayToc getGiaTienTayToc() {
		return giaTienTayToc;
	}

	public void setGiaTienTayToc(GiaTienTayToc giaTienTayToc) {
		this.giaTienTayToc = giaTienTayToc;
	}

	@ManyToOne
	public GiaTienNangNen getGiaTienNangNen() {
		return giaTienNangNen;
	}

	public void setGiaTienNangNen(GiaTienNangNen giaTienNangNen) {
		this.giaTienNangNen = giaTienNangNen;
	}
	
	@ManyToOne
	public ThoCatToc getThoCatToc() {
		return thoCatToc;
	}

	public void setThoCatToc(ThoCatToc thoCatToc) {
		this.thoCatToc = thoCatToc;
	}

	@ManyToOne
	public GiaTienUonDuoiNhuomTocNam getGiaTienUonDuoiNhuomTocNam() {
		return giaTienUonDuoiNhuomTocNam;
	}

	public void setGiaTienUonDuoiNhuomTocNam(GiaTienUonDuoiNhuomTocNam giaTienUonDuoiNhuomTocNam) {
		this.giaTienUonDuoiNhuomTocNam = giaTienUonDuoiNhuomTocNam;
	}

	@ManyToOne
	public GiaTienBamPhong getGiaTienBamPhong() {
		return giaTienBamPhong;
	}

	public void setGiaTienBamPhong(GiaTienBamPhong giaTienBamPhong) {
		this.giaTienBamPhong = giaTienBamPhong;
	}

	@ManyToOne
	public GiaTienDuoi getGiaTienDuoi() {
		return giaTienDuoi;
	}

	public void setGiaTienDuoi(GiaTienDuoi giaTienDuoi) {
		this.giaTienDuoi = giaTienDuoi;
	}

	@ManyToOne
	public GiaTienPhucHoi getGiaTienPhucHoi() {
		return giaTienPhucHoi;
	}

	public void setGiaTienPhucHoi(GiaTienPhucHoi giaTienPhucHoi) {
		this.giaTienPhucHoi = giaTienPhucHoi;
	}
	
	@ManyToOne
	public GiaTienGoi getGiaTienGoi() {
		return giaTienGoi;
	}

	public void setGiaTienGoi(GiaTienGoi giaTienGoi) {
		this.giaTienGoi = giaTienGoi;
	}

	@ManyToOne
	public GiaTienCatToc getGiaTienCatToc() {
		return giaTienCatToc;
	}

	public void setGiaTienCatToc(GiaTienCatToc giaTienCatToc) {
		this.giaTienCatToc = giaTienCatToc;
	}

	@ManyToOne
	public MaMau getMaMau() {
		return maMau;
	}

	public void setMaMau(MaMau maMau) {
		this.maMau = maMau;
	}

	@ManyToOne
	public KieuToc getKieuToc() {
		return kieuToc;
	}

	public void setKieuToc(KieuToc kieuToc) {
		this.kieuToc = kieuToc;
	}

	public String getOxy() {
		return oxy;
	}

	public void setOxy(String oxy) {
		this.oxy = oxy;
	}

	@Enumerated(EnumType.STRING)
	public DoDaiToc getDoDaiToc() {
		return doDaiToc;
	}

	public void setDoDaiToc(DoDaiToc doDaiToc) {
		this.doDaiToc = doDaiToc;
	}
	
	@Enumerated(EnumType.STRING)
	public DoDayToc getDoDayToc() {
		return doDayToc;
	}

	public void setDoDayToc(DoDayToc doDayToc) {
		this.doDayToc = doDayToc;
	}

	public String getTruc() {
		return truc;
	}

	public void setTruc(String truc) {
		this.truc = truc;
	}

	public String getNhiet() {
		return nhiet;
	}

	public void setNhiet(String nhiet) {
		this.nhiet = nhiet;
	}

	@Enumerated(EnumType.STRING)
	public LoaiDichVu getLoaiDichVu() {
		return loaiDichVu;
	}

	public void setLoaiDichVu(LoaiDichVu loaiDichVu) {
		this.loaiDichVu = loaiDichVu;
	}
	
	@Enumerated(EnumType.STRING)
	public LoaiThuoc getLoaiThuoc() {
		return loaiThuoc;
	}

	public void setLoaiThuoc(LoaiThuoc loaiThuoc) {
		this.loaiThuoc = loaiThuoc;
	}

	@ManyToOne
	@QueryInit("*.*.*")
	public LichSuKhachHang getLichSuKhachHang() {
		return lichSuKhachHang;
	}

	public void setLichSuKhachHang(LichSuKhachHang lichSuKhachHang) {
		this.lichSuKhachHang = lichSuKhachHang;
	}
	
	

	@Command
	public void saveChiTietDichVu(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {	
		LichSuKhachHang lichSu = (LichSuKhachHang) listObject;
		if (lichSu.noId()) {
			lichSu.saveNotShowNotification();
		}
		if (getLichSuKhachHang() == null) {
			setLichSuKhachHang(lichSu);
		}
		save();
		lichSu.updateTongTien();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, attr);		
	}
	
	@Command
	public void chonLoaiDichVu() {
		if (loaiDichVu.equals(LoaiDichVu.CAO_MAT)) {
			giaTien = 10000;
		} else if (loaiDichVu.equals(LoaiDichVu.RUA_MAT)) {
			giaTien = 20000;			
		} else if (loaiDichVu.equals(LoaiDichVu.SAY)) {
			giaTien = 20000;			
		} else if (loaiDichVu.equals(LoaiDichVu.TAO_KIEU)) {
			giaTien = 30000;			
		} else {
			giaTien = 0;
		}
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void chonCatToc() {
		giaTien = giaTienCatToc.getGiaTien();
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void chonPhucHoi() {
		giaTien = giaTienPhucHoi.getGiaTien();
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void chonGoi() {
		giaTien = giaTienGoi.getGiaTien();
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void chonDuoi() {
		giaTien = giaTienDuoi.getGiaTien();
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void chonBamPhong() {
		giaTien = giaTienBamPhong.getGiaTien();
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void chonUonDuoiNhuomTocNam() {
		giaTien = giaTienUonDuoiNhuomTocNam.getGiaTien();
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void chonNangNen() {
		giaTien = giaTienNangNen.getGiaTien();
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void chonTayToc() {
		giaTien = giaTienTayToc.getGiaTien();
		BindUtils.postNotifyChange(null, null, this, "giaTien");
	}
	
	@Command
	public void deleteDichVu(final @BindingParam("notify") Object beanObject) {
		if (!checkInUse()) {
			Messagebox.show("Bạn muốn xóa dịch vụ này?", "Xác nhận", Messagebox.CANCEL | Messagebox.OK,
					Messagebox.QUESTION, new EventListener<Event>() {
						@Override
						public void onEvent(final Event event) {
							if (Messagebox.ON_OK.equals(event.getName())) {
								doDelete(true);
								showNotification("Xóa thành công!", "", "success");
								((LichSuKhachHang) beanObject).updateTongTien();
								BindUtils.postNotifyChange(null, null, beanObject, "listChiTietDichVu");
							}
						}
					});
		}

	}
}
