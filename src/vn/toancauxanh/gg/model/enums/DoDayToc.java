package vn.toancauxanh.gg.model.enums;

public enum DoDayToc {
	NHIEU("Nhiều"),
	IT("Ít");

	String text;

	DoDayToc(final String txt) {
		text = txt;
	}

	public String getText() {
		return text;
	}
}
