package vn.toancauxanh.gg.model.enums;

public enum DoDaiToc {
	DAI("Dài"),
	NGAN("Ngắn");

	String text;

	DoDaiToc(final String txt) {
		text = txt;
	}

	public String getText() {
		return text;
	}
}
