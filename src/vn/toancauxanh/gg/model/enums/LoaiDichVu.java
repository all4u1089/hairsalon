package vn.toancauxanh.gg.model.enums;

public enum LoaiDichVu {
	UON("Uốn"),
	CAT_TOC("Cắt tóc"),
	PHUC_HOI("Phục hồi"),
	GOI("Gội"),
	DUOI("Duỗi"),
	BAM_PHONG("Bấm phòng"),
	UON_DUOI_NHUOM_TOC_NAM("Uốn, duỗi, nhuộm tóc nam"),
	NANG_NEN("Nâng nền"),
	CAO_MAT("Cạo mặt"),
	RUA_MAT("Rửa mặt"),
	BAN_HANG("Bán hàng"),
	TAY_TOC("Tẩy tóc"),
	HIGHLIGHT("Highlight"),
	SAY("Sấy"),
	TAO_KIEU("Tạo kiểu"),
	NHUOM("Nhuộm");

	String text;

	LoaiDichVu(final String txt) {
		text = txt;
	}

	public String getText() {
		return text;
	}
}
