package vn.toancauxanh.gg.model.enums;

public enum LoaiThuoc {
	TOT("Tốt"),
	THUONG("Thường");

	String text;

	LoaiThuoc(final String txt) {
		text = txt;
	}

	public String getText() {
		return text;
	}
}
