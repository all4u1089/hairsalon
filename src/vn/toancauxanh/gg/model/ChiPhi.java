package vn.toancauxanh.gg.model;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zul.Window;

import vn.toancauxanh.model.Model;

@Entity
@Table(name = "chiphi")
public class ChiPhi extends Model<ChiPhi>{
	
	private String ten = "";
	private String moTa = "";
	private Date ngayPhatSinh;
	private long giaTien;
	private LoaiChiTieu loaiChiTieu;
	
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getMoTa() {
		return moTa;
	}
	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}
	
	public Date getNgayPhatSinh() {
		return ngayPhatSinh;
	}
	public void setNgayPhatSinh(Date ngayPhatSinh) {
		this.ngayPhatSinh = ngayPhatSinh;
	}	
	public long getGiaTien() {
		return giaTien;
	}
	public void setGiaTien(long giaTien) {
		this.giaTien = giaTien;
	}
	@ManyToOne
	public LoaiChiTieu getLoaiChiTieu() {
		return loaiChiTieu;
	}
	public void setLoaiChiTieu(LoaiChiTieu loaiChiTieu) {
		this.loaiChiTieu = loaiChiTieu;
	}
	@Command
	public void saveChiPhi(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {	
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, attr);		
	}
	
	@Command
	public void saveChiPhiTrongNgay(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {	
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, attr);
		BindUtils.postNotifyChange(null, null, listObject, "countChiPhi");	
	}
}
