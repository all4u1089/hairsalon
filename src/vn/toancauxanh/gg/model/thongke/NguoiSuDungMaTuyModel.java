package vn.toancauxanh.gg.model.thongke;

public class NguoiSuDungMaTuyModel {
	private String tenDonVi;
	private long phatTien;
	private long quanLySauPhatTien;
	private long phatCanhCao;
	private long quanLySauPhatCanhCao;	
	private long dieuTriBangMethadone;
	private long dieuTriLoanThan;
	private long duaVaoTruongGiaoDuong;
	private long duaVaoCoSoGiaoDuc;	
	private long biXuLyHinhSuDangONhaTamGiu;
	private long biXuLyHinhSuDangOTraiTamGiam;
	private long biXuLyHinhSuDangDuocTaiNgoai;
	private long tongSo;
	public String getTenDonVi() {
		return tenDonVi;
	}
	public void setTenDonVi(String tenDonVi) {
		this.tenDonVi = tenDonVi;
	}
	public long getPhatTien() {
		return phatTien;
	}
	public void setPhatTien(long phatTien) {
		this.phatTien = phatTien;
	}
	public long getQuanLySauPhatTien() {
		return quanLySauPhatTien;
	}
	public void setQuanLySauPhatTien(long quanLySauPhatTien) {
		this.quanLySauPhatTien = quanLySauPhatTien;
	}
	public long getPhatCanhCao() {
		return phatCanhCao;
	}
	public void setPhatCanhCao(long phatCanhCao) {
		this.phatCanhCao = phatCanhCao;
	}
	public long getQuanLySauPhatCanhCao() {
		return quanLySauPhatCanhCao;
	}
	public void setQuanLySauPhatCanhCao(long quanLySauPhatCanhCao) {
		this.quanLySauPhatCanhCao = quanLySauPhatCanhCao;
	}
	public long getDieuTriBangMethadone() {
		return dieuTriBangMethadone;
	}
	public void setDieuTriBangMethadone(long dieuTriBangMethadone) {
		this.dieuTriBangMethadone = dieuTriBangMethadone;
	}
	public long getDieuTriLoanThan() {
		return dieuTriLoanThan;
	}
	public void setDieuTriLoanThan(long dieuTriLoanThan) {
		this.dieuTriLoanThan = dieuTriLoanThan;
	}
	public long getDuaVaoTruongGiaoDuong() {
		return duaVaoTruongGiaoDuong;
	}
	public void setDuaVaoTruongGiaoDuong(long duaVaoTruongGiaoDuong) {
		this.duaVaoTruongGiaoDuong = duaVaoTruongGiaoDuong;
	}
	public long getDuaVaoCoSoGiaoDuc() {
		return duaVaoCoSoGiaoDuc;
	}
	public void setDuaVaoCoSoGiaoDuc(long duaVaoCoSoGiaoDuc) {
		this.duaVaoCoSoGiaoDuc = duaVaoCoSoGiaoDuc;
	}
	public long getBiXuLyHinhSuDangONhaTamGiu() {
		return biXuLyHinhSuDangONhaTamGiu;
	}
	public void setBiXuLyHinhSuDangONhaTamGiu(long biXuLyHinhSuDangONhaTamGiu) {
		this.biXuLyHinhSuDangONhaTamGiu = biXuLyHinhSuDangONhaTamGiu;
	}
	public long getBiXuLyHinhSuDangOTraiTamGiam() {
		return biXuLyHinhSuDangOTraiTamGiam;
	}
	public void setBiXuLyHinhSuDangOTraiTamGiam(long biXuLyHinhSuDangOTraiTamGiam) {
		this.biXuLyHinhSuDangOTraiTamGiam = biXuLyHinhSuDangOTraiTamGiam;
	}
	public long getBiXuLyHinhSuDangDuocTaiNgoai() {
		return biXuLyHinhSuDangDuocTaiNgoai;
	}
	public void setBiXuLyHinhSuDangDuocTaiNgoai(long biXuLyHinhSuDangDuocTaiNgoai) {
		this.biXuLyHinhSuDangDuocTaiNgoai = biXuLyHinhSuDangDuocTaiNgoai;
	}
	public long getTongSo() {
		return tongSo;
	}
	public void setTongSo(long tongSo) {
		this.tongSo = tongSo;
	}
}
