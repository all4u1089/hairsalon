package vn.toancauxanh.gg.model.thongke;

import java.util.Date;

public class KhoangThoiGianTimKiem {
	public KhoangThoiGianTimKiem() {
	}
	
	public KhoangThoiGianTimKiem(String name, Date tuNgay, Date denNgay) {
		this.name = name;
		this.tuNgay = tuNgay;
		this.denNgay = denNgay;
	}
	private String name;
	private Date tuNgay;
	private Date denNgay;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getTuNgay() {
		return tuNgay;
	}
	public void setTuNgay(Date tuNgay) {
		this.tuNgay = tuNgay;
	}
	public Date getDenNgay() {
		return denNgay;
	}
	public void setDenNgay(Date denNgay) {
		this.denNgay = denNgay;
	}
}
