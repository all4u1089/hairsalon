package vn.toancauxanh.gg.model.thongke;

public class NguoiNghienModel {
	private String tenDonVi;
	private long tapTrungCaiNghienBatBuoc;
	private long tapTrungCaiNghienTuNguyen;
	private long quanLySauCai;
	private long dieuTriBangMethadone;
	private long dieuTriLoanThan;
	private long giaoDucTaiXaPhuong;
	private long quanLySauGiaoDucTaiXaPhuong;
	private long caiNghienTuNguyenTaiGiaDinh;
	private long caiNghienTuNguyenTaiCongDong;
	private long caiNghienBatBuocTaiCongDong;
	private long duaVaoTruongGiaoDuong;
	private long duaVaoCoSoGiaoDuc;
	private long biXuLyHinhSuDangONhaTamGiu;
	private long biXuLyHinhSuDangOTraiTamGiam;
	private long biXuLyHinhSuDangDuocTaiNgoai;
	private long tongSo;
	public String getTenDonVi() {
		return tenDonVi;
	}
	public void setTenDonVi(String tenDonVi) {
		this.tenDonVi = tenDonVi;
	}
	public long getTapTrungCaiNghienBatBuoc() {
		return tapTrungCaiNghienBatBuoc;
	}
	public void setTapTrungCaiNghienBatBuoc(long tapTrungCaiNghienBatBuoc) {
		this.tapTrungCaiNghienBatBuoc = tapTrungCaiNghienBatBuoc;
	}
	public long getTapTrungCaiNghienTuNguyen() {
		return tapTrungCaiNghienTuNguyen;
	}
	public void setTapTrungCaiNghienTuNguyen(long tapTrungCaiNghienTuNguyen) {
		this.tapTrungCaiNghienTuNguyen = tapTrungCaiNghienTuNguyen;
	}
	public long getQuanLySauCai() {
		return quanLySauCai;
	}
	public void setQuanLySauCai(long quanLySauCai) {
		this.quanLySauCai = quanLySauCai;
	}
	public long getDieuTriBangMethadone() {
		return dieuTriBangMethadone;
	}
	public void setDieuTriBangMethadone(long dieuTriBangMethadone) {
		this.dieuTriBangMethadone = dieuTriBangMethadone;
	}
	public long getDieuTriLoanThan() {
		return dieuTriLoanThan;
	}
	public void setDieuTriLoanThan(long dieuTriLoanThan) {
		this.dieuTriLoanThan = dieuTriLoanThan;
	}
	public long getGiaoDucTaiXaPhuong() {
		return giaoDucTaiXaPhuong;
	}
	public void setGiaoDucTaiXaPhuong(long giaoDucTaiXaPhuong) {
		this.giaoDucTaiXaPhuong = giaoDucTaiXaPhuong;
	}
	public long getQuanLySauGiaoDucTaiXaPhuong() {
		return quanLySauGiaoDucTaiXaPhuong;
	}
	public void setQuanLySauGiaoDucTaiXaPhuong(long quanLySauGiaoDucTaiXaPhuong) {
		this.quanLySauGiaoDucTaiXaPhuong = quanLySauGiaoDucTaiXaPhuong;
	}
	public long getCaiNghienTuNguyenTaiGiaDinh() {
		return caiNghienTuNguyenTaiGiaDinh;
	}
	public void setCaiNghienTuNguyenTaiGiaDinh(long caiNghienTuNguyenTaiGiaDinh) {
		this.caiNghienTuNguyenTaiGiaDinh = caiNghienTuNguyenTaiGiaDinh;
	}
	public long getCaiNghienTuNguyenTaiCongDong() {
		return caiNghienTuNguyenTaiCongDong;
	}
	public void setCaiNghienTuNguyenTaiCongDong(long caiNghienTuNguyenTaiCongDong) {
		this.caiNghienTuNguyenTaiCongDong = caiNghienTuNguyenTaiCongDong;
	}
	public long getCaiNghienBatBuocTaiCongDong() {
		return caiNghienBatBuocTaiCongDong;
	}
	public void setCaiNghienBatBuocTaiCongDong(long caiNghienBatBuocTaiCongDong) {
		this.caiNghienBatBuocTaiCongDong = caiNghienBatBuocTaiCongDong;
	}
	public long getDuaVaoTruongGiaoDuong() {
		return duaVaoTruongGiaoDuong;
	}
	public void setDuaVaoTruongGiaoDuong(long duaVaoTruongGiaoDuong) {
		this.duaVaoTruongGiaoDuong = duaVaoTruongGiaoDuong;
	}
	public long getDuaVaoCoSoGiaoDuc() {
		return duaVaoCoSoGiaoDuc;
	}
	public void setDuaVaoCoSoGiaoDuc(long duaVaoCoSoGiaoDuc) {
		this.duaVaoCoSoGiaoDuc = duaVaoCoSoGiaoDuc;
	}
	public long getBiXuLyHinhSuDangONhaTamGiu() {
		return biXuLyHinhSuDangONhaTamGiu;
	}
	public void setBiXuLyHinhSuDangONhaTamGiu(long biXuLyHinhSuDangONhaTamGiu) {
		this.biXuLyHinhSuDangONhaTamGiu = biXuLyHinhSuDangONhaTamGiu;
	}
	public long getBiXuLyHinhSuDangOTraiTamGiam() {
		return biXuLyHinhSuDangOTraiTamGiam;
	}
	public void setBiXuLyHinhSuDangOTraiTamGiam(long biXuLyHinhSuDangOTraiTamGiam) {
		this.biXuLyHinhSuDangOTraiTamGiam = biXuLyHinhSuDangOTraiTamGiam;
	}
	public long getBiXuLyHinhSuDangDuocTaiNgoai() {
		return biXuLyHinhSuDangDuocTaiNgoai;
	}
	public void setBiXuLyHinhSuDangDuocTaiNgoai(long biXuLyHinhSuDangDuocTaiNgoai) {
		this.biXuLyHinhSuDangDuocTaiNgoai = biXuLyHinhSuDangDuocTaiNgoai;
	}
	public long getTongSo() {
		return tongSo;
	}
	public void setTongSo(long tongSo) {
		this.tongSo = tongSo;
	}
}
