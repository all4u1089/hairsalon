package vn.toancauxanh.gg.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zul.Window;

import com.querydsl.jpa.impl.JPAQuery;

import vn.toancauxanh.gg.model.enums.LoaiDichVu;
import vn.toancauxanh.model.Model;

@Entity
@Table(name = "lichsukhachhang")
public class LichSuKhachHang extends Model<LichSuKhachHang>{
	
	private KhachHang khachHang;
	private Date ngaySuDung;
	private long tongTien;
	private long tongTienThanhToan;
	private KhachHang khachHangKhuyenMai;
	private long khuyenMai;
	private long phanTramKhuyenMai;
	private List<ChiTietDichVu> chiTietDichVus = new ArrayList<ChiTietDichVu>();
	
	@OneToMany(mappedBy = "lichSuKhachHang", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	public List<ChiTietDichVu> getChiTietDichVus() {
		return chiTietDichVus;
	}

	public void setChiTietDichVus(List<ChiTietDichVu> chiTietDichVus) {
		this.chiTietDichVus = chiTietDichVus;
	}

	@ManyToOne
	public KhachHang getKhachHang() {
		return khachHang;
	}

	public void setKhachHang(KhachHang khachHang) {
		this.khachHang = khachHang;
	}
	
	@ManyToOne
	public KhachHang getKhachHangKhuyenMai() {
		return khachHangKhuyenMai;
	}

	public void setKhachHangKhuyenMai(KhachHang khachHangKhuyenMai) {
		this.khachHangKhuyenMai = khachHangKhuyenMai;
	}

	public Date getNgaySuDung() {
		return ngaySuDung;
	}

	public void setNgaySuDung(Date ngaySuDung) {
		this.ngaySuDung = ngaySuDung;
	}
	
	public long getTongTienThanhToan() {
		return tongTienThanhToan;
	}

	public void setTongTienThanhToan(long tongTienThanhToan) {
		this.tongTienThanhToan = tongTienThanhToan;
	}

	public long getTongTien() {
		return tongTien;
	}

	public void setTongTien(long tongTien) {
		this.tongTien = tongTien;
	}
	
	public long getPhanTramKhuyenMai() {
		return phanTramKhuyenMai;
	}

	public void setPhanTramKhuyenMai(long phanTramKhuyenMai) {
		this.phanTramKhuyenMai = phanTramKhuyenMai;
	}

	public long getKhuyenMai() {
		return khuyenMai;
	}
	
	public void setKhuyenMai(long khuyenMai) {
		this.khuyenMai = khuyenMai;
	}

	@Command
	public void saveLichSuKhachHang(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {	
		if (khachHangKhuyenMai != null) {
			if (!khachHangKhuyenMai.isDaDungKMGioiThieu()) {
				khachHangKhuyenMai.setDaDungKMGioiThieu(true);
				khachHangKhuyenMai.saveNotShowNotification();
				BindUtils.postNotifyChange(null, null, listObject,"listNguoiGioiThieu");
			}
		}
		updateTongTien();
		save();
		wdn.detach();
		BindUtils.postNotifyChange(null, null, listObject, attr);		
	}
	
	@Command
	public void saveLichSuKhachHangTrongNgay(@BindingParam("list") final Object listObject, @BindingParam("attr") final String attr,
			@BindingParam("wdn") final Window wdn) throws IOException {	
		if (khachHangKhuyenMai != null) {
			if (!khachHangKhuyenMai.isDaDungKMGioiThieu()) {
				khachHangKhuyenMai.setDaDungKMGioiThieu(true);
				khachHangKhuyenMai.saveNotShowNotification();
				BindUtils.postNotifyChange(null, null, listObject,"listNguoiGioiThieu");
			}
		}
		updateTongTien();
		save();
		wdn.detach();	
		BindUtils.postNotifyChange(null, null, listObject, attr);	
		BindUtils.postNotifyChange(null, null, listObject, "countDoanhThu");
	}	
	
	@Transient
	public List<ChiTietDichVu> getListChiTietDichVu() {
		List<ChiTietDichVu> list = new ArrayList<ChiTietDichVu>();
		if (!noId()) {
			list = find(ChiTietDichVu.class)
					.where(QChiTietDichVu.chiTietDichVu.lichSuKhachHang.eq(this))
					.fetch();
		}		
		return list;
	}
	
	@Transient
	public String getListChiTietDichVuStr() {
		String out = "";
		for (ChiTietDichVu chiTiet : getListChiTietDichVu()) {
			out += ", " + chiTiet.getLoaiDichVu().getText();
		}
		return out.replaceFirst(", ", "");
	}
	
	@Transient
	public String getListChiTietDichVuStr(ThoCatToc thoCatToc) {
		String out = "";
		if (thoCatToc != null) {
			for (ChiTietDichVu chiTiet : getListChiTietDichVu()) {
				if (chiTiet.getThoCatToc().getId().equals(thoCatToc.getId())) {
					out += ", " + chiTiet.getLoaiDichVu().getText();
				}
			}
		}
		return out.replaceFirst(", ", "");
	}
	
	@Transient
	public List<KhachHang> getListKhuyenMai() {
		List<KhachHang> list = new ArrayList<>();
		if (getKhachHang() != null) {
			list = find(KhachHang.class)
					.where(QKhachHang.khachHang.gioiThieu.eq(getKhachHang()))
					.where(QKhachHang.khachHang.daDungKMGioiThieu.eq(false))
					.fetch();
		}		
		return list;
	}
	
	@Transient
	public List<KhachHang> getListKhuyenMaiAndNull() {
		List<KhachHang> list = new ArrayList<>();
		list.add(null);
		list.addAll(getListKhuyenMai());
		return list;
	}
	
	@Command
	public void updateTongTien() {
		tongTien = 0;
		khuyenMai = 0;
		for (ChiTietDichVu dv : getListChiTietDichVu()) {
			tongTien += dv.getGiaTien();
		}
		if (khachHangKhuyenMai != null) {
			for (ChiTietDichVu dv : getListChiTietDichVu()) {
				if (!dv.getLoaiDichVu().equals(LoaiDichVu.BAN_HANG)) {
					khuyenMai += (dv.getGiaTien() * 0.1);
				}
			}
		} else if (phanTramKhuyenMai > 0) {
			for (ChiTietDichVu dv : getListChiTietDichVu()) {
				if (!dv.getLoaiDichVu().equals(LoaiDichVu.BAN_HANG)) {
					khuyenMai += (dv.getGiaTien() * phanTramKhuyenMai)/100;
				}
			}
		}
		tongTienThanhToan = tongTien - khuyenMai;
		BindUtils.postNotifyChange(null, null, this, "tongTien");
		BindUtils.postNotifyChange(null, null, this, "khuyenMai");
		BindUtils.postNotifyChange(null, null, this, "tongTienThanhToan");
	}
	
	@Transient
	public long getTongTienTheoNhanVien(ThoCatToc thoCatToc) {
		long tongTienNV = 0;
		long khuyenMaiNV = 0;
		if (thoCatToc != null) {
			for (ChiTietDichVu dv : getListChiTietDichVu()) {
				if (dv.getThoCatToc().getId().equals(thoCatToc.getId())) {
					tongTienNV += dv.getGiaTien();
				}
			}
			if (khachHangKhuyenMai != null) {
				for (ChiTietDichVu dv : getListChiTietDichVu()) {
					if (!dv.getLoaiDichVu().equals(LoaiDichVu.BAN_HANG) && dv.getThoCatToc().getId().equals(thoCatToc.getId())) {
						khuyenMaiNV += (dv.getGiaTien() * 0.1);
					}
				}
			} else if (phanTramKhuyenMai > 0) {
				for (ChiTietDichVu dv : getListChiTietDichVu()) {
					if (!dv.getLoaiDichVu().equals(LoaiDichVu.BAN_HANG) && dv.getThoCatToc().getId().equals(thoCatToc.getId())) {
						khuyenMaiNV += (dv.getGiaTien() * phanTramKhuyenMai)/100;
					}
				}
			}
		}
		return tongTienNV - khuyenMaiNV;
	}
	
	private List<KhachHang> tacGiaTimKiems = new ArrayList<KhachHang>();
	
	@Transient
	public List<KhachHang> getTacGiaTimKiems() {
		if (tacGiaTimKiems.isEmpty()) {
			tacGiaTimKiems.addAll(find(KhachHang.class)
					.where(QKhachHang.khachHang.trangThai.eq(core().TT_AP_DUNG))
					.fetch());			
		}
		return tacGiaTimKiems;
	}
	
	@Command
	public void timKiemTacGia(@BindingParam("tenTacGia") final String tenTacGia) {
		if (tenTacGia.isEmpty()) {
			tacGiaTimKiems.clear();
			tacGiaTimKiems.addAll(find(KhachHang.class)
					.where(QKhachHang.khachHang.trangThai.eq(core().TT_AP_DUNG))
					.fetch());
		} else {
			tacGiaTimKiems.clear();			
			JPAQuery<KhachHang> q = find(KhachHang.class)
					.where(QKhachHang.khachHang.trangThai.eq(core().TT_AP_DUNG))					
					.where(QKhachHang.khachHang.hoVaTen.containsIgnoreCase(tenTacGia)
							.or(QKhachHang.khachHang.tenFacebook.containsIgnoreCase(tenTacGia)));			
			
			if (q.fetchCount() > 0) {
				tacGiaTimKiems.addAll(q.orderBy(QKhachHang.khachHang.hoVaTen.asc()).fetch());
			} else {
				tacGiaTimKiems.add(null);
			}			
		}
		BindUtils.postNotifyChange(null, null, this, "tacGiaTimKiems");
	}
}
